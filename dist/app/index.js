"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ejs_module_1 = require("./controller/demo/ejs.module");
const index_module_1 = require("./controller/index/index.module");
const login_module_1 = require("./controller/login/login.module");
const register_module_1 = require("./controller/register/register.module");
const posts_module_1 = require("./controller/posts/posts.module");
const publish_module_1 = require("./controller/publish/publish.module");
const account_module_1 = require("./controller/account/account.module");
const setting_module_1 = require("./controller/setting/setting.module");
const reply_module_1 = require("./controller/reply/reply.module");
const articles_module_1 = require("./controller/articles/articles.module");
const articleDetail_module_1 = require("./controller/articleDetail/articleDetail.module");
const collect_module_1 = require("./controller/collect/collect.module");
const searchPage_module_1 = require("./controller/searchPage/searchPage.module");
const auth_module_1 = require("./controller/auth/auth.module");
const update_module_1 = require("./controller/download/update.module");
const modules = [
    ejs_module_1.EJSModule,
    index_module_1.IndexModule,
    login_module_1.LoginModule,
    register_module_1.RegisterModule,
    posts_module_1.PostsModule,
    account_module_1.AccountModule,
    publish_module_1.PublishModule,
    setting_module_1.SettingModule,
    reply_module_1.ReplyModule,
    articles_module_1.ArticlesModule,
    articleDetail_module_1.ArticleDetailModule,
    collect_module_1.CollectModule,
    searchPage_module_1.SearchPageModule,
    auth_module_1.AuthModule,
    update_module_1.UpdateModule,
];
exports.default = modules;
//# sourceMappingURL=index.js.map