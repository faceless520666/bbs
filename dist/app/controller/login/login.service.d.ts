import { Repository } from 'typeorm';
import { BbsUser } from '../../entitys/user.entity';
export declare class LoginService {
    private readonly signRepository;
    user: BbsUser;
    constructor(signRepository: Repository<BbsUser>);
    createToken(userId: any): Promise<any>;
    login(data: any): Promise<any>;
    Emailverifica(param: any): Promise<any>;
}
