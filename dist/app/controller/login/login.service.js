"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const api_error_code_enum_1 = require("../../../bing/common/enums/api-error-code.enum");
const typeorm_2 = require("typeorm");
const user_entity_1 = require("../../entitys/user.entity");
const encrypt_1 = require("../../../bing/common/encrypt");
const send_e_mail_1 = require("../register/e-mail/send.e-mail");
const jwt = require("jsonwebtoken");
const bing_1 = require("../../../bing");
let LoginService = class LoginService {
    constructor(signRepository) {
        this.signRepository = signRepository;
    }
    createToken(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            const user = { userId };
            return jwt.sign({
                user,
            }, bing_1.util.session.secrets, {
                expiresIn: '3600s',
            });
        });
    }
    login(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const user = { userId: data.userId };
            const res = yield this.signRepository.findOne({ USER_ID: data.userId });
            const msg = {
                code: 1,
                message: '',
                HttpStatus: 200,
                data: {},
            };
            if (res) {
                if (encrypt_1.md5(data.passWord) === res.PASSWORD) {
                    const token = yield this.createToken(data.userId);
                    console.log(token);
                    msg.code = api_error_code_enum_1.ApiErrorCode.USER_LOGIN_SUCCESS;
                    msg.message = '登入成功';
                    const data1 = {
                        userId: res.USER_ID,
                        nickName: res.NICK_NAME,
                        token,
                    };
                    msg.data = data1;
                    console.log(111, msg);
                    return msg;
                }
                if (encrypt_1.md5(data.passWord) !== res.PASSWORD) {
                    msg.code = api_error_code_enum_1.ApiErrorCode.USER_PASSWORD_ERROR;
                    msg.HttpStatus = common_1.HttpStatus.BAD_REQUEST;
                    msg.message = '用户密码不正确！';
                    return msg;
                }
            }
            else {
                msg.code = api_error_code_enum_1.ApiErrorCode.USER_ID_INVALID;
                msg.HttpStatus = common_1.HttpStatus.BAD_REQUEST;
                msg.message = '用户不存在，请注册！';
                return msg;
            }
        });
    }
    Emailverifica(param) {
        return __awaiter(this, void 0, void 0, function* () {
            const date = yield this.signRepository.findOne({ USER_ID: param.userId });
            if (date) {
                const result = yield send_e_mail_1.Verification.Everifica(param.userId, param);
                console.log(result.code);
                if (result.code === 10008) {
                    date.ACTIVITY = true;
                    yield this.signRepository.save(date);
                }
                return result;
            }
            else {
                console.log('邮箱验证：', param);
                return { message: '用有误' };
            }
        });
    }
};
LoginService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(user_entity_1.BbsUser)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], LoginService);
exports.LoginService = LoginService;
//# sourceMappingURL=login.service.js.map