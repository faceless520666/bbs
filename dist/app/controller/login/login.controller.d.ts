import { LoginService } from './login.service';
import { BbsUser } from '../../entitys/user.entity';
export declare class LoginController {
    private readonly loginService;
    constructor(loginService: LoginService);
    index(req: any, res: any): Promise<any>;
    login(params: any): Promise<BbsUser[]>;
    verifica(req: any, res: any): Promise<any>;
    Everifica(param: any): Promise<any>;
}
