import { Repository } from 'typeorm';
import { BbsCommentsList } from '../../entitys/commentList.entity';
import { BbsChildrenComments } from '../../entitys/childrenComment.entity';
import { BbsUser } from '../../entitys/user.entity';
export declare class ReplyService {
    private readonly commentRepository;
    private readonly childrenCommentRepository;
    private readonly userCommentRepository;
    constructor(commentRepository: Repository<BbsCommentsList>, childrenCommentRepository: Repository<BbsChildrenComments>, userCommentRepository: Repository<BbsUser>);
    getComments(param: any): Promise<any>;
}
