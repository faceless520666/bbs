import { ReplyService } from './reply.service';
export declare class ReplyController {
    private readonly replyService;
    constructor(replyService: ReplyService);
    index(req: any, res: any, data: any): Promise<any>;
    getCommentPage(data: any): Promise<any>;
}
