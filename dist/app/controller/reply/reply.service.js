"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const api_error_code_enum_1 = require("../../../bing/common/enums/api-error-code.enum");
const typeorm_2 = require("typeorm");
const commentList_entity_1 = require("../../entitys/commentList.entity");
const childrenComment_entity_1 = require("../../entitys/childrenComment.entity");
const user_entity_1 = require("../../entitys/user.entity");
let ReplyService = class ReplyService {
    constructor(commentRepository, childrenCommentRepository, userCommentRepository) {
        this.commentRepository = commentRepository;
        this.childrenCommentRepository = childrenCommentRepository;
        this.userCommentRepository = userCommentRepository;
    }
    getComments(param) {
        return __awaiter(this, void 0, void 0, function* () {
            let commentRes;
            const pageCount = param.pageCount ? param.pageCount * 1 : 5;
            const page = param.page ? (param.page - 1) * 1 * pageCount : 0;
            const user = yield this.userCommentRepository.findOne({ NICK_NAME: param.nickName });
            user.HAD_NEWS = false;
            this.userCommentRepository.save(user);
            const totalRes = yield this.commentRepository.find({ COMMENTATOR_ID: user.USER_ID });
            commentRes = yield this.commentRepository
                .createQueryBuilder('commentleList')
                .where('commentleList.COMMENTATOR_ID = :COMMENTATOR_ID', { COMMENTATOR_ID: user.USER_ID })
                .orWhere('commentleList.COMMENTATOR_NAME= :COMMENTATOR_NAME', { COMMENTATOR_NAME: param.nickName })
                .orderBy('commentleList.ID', 'DESC')
                .skip(page)
                .take(pageCount)
                .getMany();
            for (const item of commentRes) {
                const filterMyAnswer = [];
                const commentUserInfo = yield this.userCommentRepository.findOne({ USER_ID: item.USER_ID });
                item.hearderIcon = commentUserInfo.HEADER_ICON;
                const childrenComRes = yield this.childrenCommentRepository
                    .createQueryBuilder('childCommentList')
                    .where('childCommentList.ARTICLE_ID = :ARTICLE_ID', { ARTICLE_ID: item.articleId })
                    .andWhere('childCommentList.COMMENT_ID = :COMMENT_ID', { COMMENT_ID: item.commentId })
                    .getMany();
                childrenComRes.forEach((chilItem, chilInd) => {
                    if (chilItem.USER_ID !== user.USER_ID) {
                        filterMyAnswer.push(chilItem);
                    }
                });
                item.childrenComentList = filterMyAnswer;
            }
            const resData = {
                code: api_error_code_enum_1.ApiErrorCode.SUCCESS,
                commentRes,
                total: totalRes.length,
            };
            return resData;
        });
    }
};
ReplyService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(commentList_entity_1.BbsCommentsList)),
    __param(1, typeorm_1.InjectRepository(childrenComment_entity_1.BbsChildrenComments)),
    __param(2, typeorm_1.InjectRepository(user_entity_1.BbsUser)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository])
], ReplyService);
exports.ReplyService = ReplyService;
//# sourceMappingURL=reply.service.js.map