"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const swagger_1 = require("@nestjs/swagger");
const dto_base_1 = require("./dto.base");
class TransferDto extends dto_base_1.BaseDto {
    constructor() {
        super(...arguments);
        this.type = 0;
        this.contract = false;
        this.concurrent = 1;
        this.nonce = 0;
    }
}
__decorate([
    swagger_1.ApiModelProperty({ description: 'from address' }),
    __metadata("design:type", String)
], TransferDto.prototype, "from", void 0);
__decorate([
    swagger_1.ApiModelProperty({ description: 'to address' }),
    __metadata("design:type", String)
], TransferDto.prototype, "to", void 0);
__decorate([
    swagger_1.ApiModelProperty({ description: 'amount' }),
    __metadata("design:type", String)
], TransferDto.prototype, "amount", void 0);
__decorate([
    swagger_1.ApiModelProperty({ description: '0 -> normal || 1 -> be candidate(from == to) || 2 -> quit candidate(from == to) || 3 -> from vote to || 4 -> from cancel vote to' }),
    __metadata("design:type", Number)
], TransferDto.prototype, "type", void 0);
__decorate([
    swagger_1.ApiModelProperty({ description: 'is contract' }),
    __metadata("design:type", Boolean)
], TransferDto.prototype, "contract", void 0);
__decorate([
    swagger_1.ApiModelProperty({ description: 'concurrent' }),
    __metadata("design:type", Number)
], TransferDto.prototype, "concurrent", void 0);
__decorate([
    swagger_1.ApiModelProperty({ description: 'nonce' }),
    __metadata("design:type", Number)
], TransferDto.prototype, "nonce", void 0);
exports.TransferDto = TransferDto;
//# sourceMappingURL=ejs.transfer.dto.js.map