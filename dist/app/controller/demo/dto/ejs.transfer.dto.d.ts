import { BaseDto } from './dto.base';
export declare class TransferDto extends BaseDto {
    from: string;
    to: string;
    amount: string;
    type: number;
    contract: boolean;
    concurrent: number;
    nonce: number;
}
