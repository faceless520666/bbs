import { EJSService } from './ejs.service';
import { TransferDto } from './dto';
export declare class DposController {
    private readonly ejsService;
    constructor(ejsService: EJSService);
    index(req: any, res: any): Promise<any>;
    users(dto: TransferDto): Promise<any>;
}
