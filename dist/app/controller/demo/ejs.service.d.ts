import { TransferDto } from './dto';
export declare class EJSService {
    transfer(data: TransferDto): Promise<any>;
}
