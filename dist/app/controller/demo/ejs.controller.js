"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const ejs_service_1 = require("./ejs.service");
const dto_1 = require("./dto");
let DposController = class DposController {
    constructor(ejsService) {
        this.ejsService = ejsService;
    }
    index(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            res.render('users', { title: 'users', name: 'Tom' });
        });
    }
    users(dto) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.ejsService.transfer(dto);
        });
    }
};
__decorate([
    common_1.Get('/'),
    swagger_1.ApiOperation({ title: 'get balance from address' }),
    __param(0, common_1.Request()), __param(1, common_1.Response()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], DposController.prototype, "index", null);
__decorate([
    common_1.Post('ransfer'),
    swagger_1.ApiOperation({ title: 'get balance from address' }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.TransferDto]),
    __metadata("design:returntype", Promise)
], DposController.prototype, "users", null);
DposController = __decorate([
    swagger_1.ApiUseTags('ejs'),
    common_1.Controller('ejs'),
    __metadata("design:paramtypes", [ejs_service_1.EJSService])
], DposController);
exports.DposController = DposController;
//# sourceMappingURL=ejs.controller.js.map