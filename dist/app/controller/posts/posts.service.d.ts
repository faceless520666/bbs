import { Repository } from 'typeorm';
import { BbsPostList } from '../../entitys/postList.entity';
import { BbsArticleDetail } from '../../entitys/articleDetail.entity';
import { BbsUser } from '../../entitys/user.entity';
export declare class PostsService {
    private readonly postsRepository;
    private readonly articleRepository;
    private readonly usereRepository;
    constructor(postsRepository: Repository<BbsPostList>, articleRepository: Repository<BbsArticleDetail>, usereRepository: Repository<BbsUser>);
    myArticleList(data: any): Promise<any>;
    deleteArticle(data: any): Promise<any>;
}
