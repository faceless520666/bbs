"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const api_error_code_enum_1 = require("../../../bing/common/enums/api-error-code.enum");
const typeorm_2 = require("typeorm");
const postList_entity_1 = require("../../entitys/postList.entity");
const articleDetail_entity_1 = require("../../entitys/articleDetail.entity");
const user_entity_1 = require("../../entitys/user.entity");
let PostsService = class PostsService {
    constructor(postsRepository, articleRepository, usereRepository) {
        this.postsRepository = postsRepository;
        this.articleRepository = articleRepository;
        this.usereRepository = usereRepository;
    }
    myArticleList(data) {
        return __awaiter(this, void 0, void 0, function* () {
            let res;
            let totalRes;
            const pageCount = data.pageCount ? data.pageCount * 1 : 10;
            const page = data.page ? (data.page - 1) * 1 * pageCount : 0;
            const user = yield this.usereRepository.findOne({ NICK_NAME: data.nickName });
            totalRes = yield this.postsRepository.find({ USER_ID: user.USER_ID });
            res = yield this.postsRepository
                .createQueryBuilder('articleList')
                .where('articleList.USER_ID = :USER_ID', { USER_ID: user.USER_ID })
                .orderBy('articleList.ID', 'DESC')
                .skip(page)
                .take(pageCount)
                .getMany();
            console.log(111, res);
            const articleData = {
                articleList: res,
                total: totalRes.length,
            };
            return articleData;
        });
    }
    deleteArticle(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const connection = typeorm_2.getConnection();
            const queryRunner = connection.createQueryRunner();
            yield queryRunner.connect();
            yield queryRunner.startTransaction();
            try {
                const res = yield this.postsRepository.find({ ARTICLE_ID: data.articleId });
                const result = yield this.postsRepository.remove(res);
                if (result.length > 0) {
                    const articlkeRes = yield this.articleRepository.find({ ARTICLE_ID: data.articleId });
                    yield this.articleRepository.remove(articlkeRes);
                    yield queryRunner.commitTransaction();
                    return { message: '删除成功！' };
                }
                else {
                    const msg = {
                        code: api_error_code_enum_1.ApiErrorCode.REMOVE_FAILT,
                        HttpStatus: common_1.HttpStatus.BAD_REQUEST,
                        message: '删除失败',
                    };
                    yield queryRunner.commitTransaction();
                    return msg;
                }
            }
            catch (err) {
                yield queryRunner.rollbackTransaction();
            }
            finally {
                yield queryRunner.release();
            }
        });
    }
};
PostsService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(postList_entity_1.BbsPostList)),
    __param(1, typeorm_1.InjectRepository(articleDetail_entity_1.BbsArticleDetail)),
    __param(2, typeorm_1.InjectRepository(user_entity_1.BbsUser)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository])
], PostsService);
exports.PostsService = PostsService;
//# sourceMappingURL=posts.service.js.map