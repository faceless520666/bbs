import { PostsService } from './posts.service';
export declare class PostsController {
    private readonly postsService;
    constructor(postsService: PostsService);
    index(req: any, res: any, data: any): Promise<any>;
    getArticle(param: any): Promise<any>;
    deleteArticle(data: any): Promise<any>;
}
