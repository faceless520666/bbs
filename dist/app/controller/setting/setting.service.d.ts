import { Repository } from 'typeorm';
import { BbsUser } from '../../entitys/user.entity';
export declare class SettingService {
    private readonly settingRepository;
    constructor(settingRepository: Repository<BbsUser>);
    getUserInfo(param: any): Promise<any>;
    resetPassword(param: any): Promise<any>;
    resetEmail(param: any): Promise<any>;
    validateEmail(param: any): Promise<any>;
}
