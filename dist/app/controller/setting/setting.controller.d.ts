import { SettingService } from './setting.service';
export declare class SettingController {
    private readonly settingService;
    constructor(settingService: SettingService);
    index(req: any, res: any, param: any): Promise<any>;
    lose(req: any, res: any, param: any): Promise<any>;
    resetPassword(params: any): Promise<Account[]>;
    resetEmail(params: any): Promise<Account[]>;
    validateEmail(params: any): Promise<Account[]>;
    getUserInfo(params: any): Promise<any>;
}
