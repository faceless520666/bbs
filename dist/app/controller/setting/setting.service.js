"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const api_error_code_enum_1 = require("../../../bing/common/enums/api-error-code.enum");
const typeorm_2 = require("typeorm");
const user_entity_1 = require("../../entitys/user.entity");
const encrypt_1 = require("../../../bing/common/encrypt");
const send_e_mail_1 = require("../register/e-mail/send.e-mail");
const jwt = require("jsonwebtoken");
const bing_1 = require("../../../bing");
const code = require("../../code");
let SettingService = class SettingService {
    constructor(settingRepository) {
        this.settingRepository = settingRepository;
    }
    getUserInfo(param) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log(yield this.settingRepository.findOne({ USER_ID: param.userId }));
            return yield this.settingRepository.findOne({ USER_ID: param.userId });
        });
    }
    resetPassword(param) {
        return __awaiter(this, void 0, void 0, function* () {
            const connection = typeorm_2.getConnection();
            const queryRunner = connection.createQueryRunner();
            yield queryRunner.connect();
            yield queryRunner.startTransaction();
            try {
                if (param.passWordOne !== param.passWordTwo) {
                    return { message: '两次密码不一致！!' };
                }
                const res = yield this.settingRepository.findOne({ USER_ID: param.userId });
                const msg = {
                    code: 1,
                    message: '',
                    HttpStatus: 200,
                    data: {},
                };
                if (!param.code && encrypt_1.md5(param.oldPassword) !== res.PASSWORD) {
                    msg.code = api_error_code_enum_1.ApiErrorCode.USER_PASSWORD_ERROR;
                    msg.HttpStatus = common_1.HttpStatus.BAD_REQUEST;
                    msg.message = '输入的旧密码不正确！';
                    return msg;
                }
                else {
                    console.log(param);
                    let b = code.default.code;
                    console.log(1111111, b);
                }
                const authToken = jwt.sign({ userId: param.userId, exp: (Date.now() / 1000) + (60 * 2) }, bing_1.util.session.secrets);
                res.PASSWORD = encrypt_1.md5(param.passWordOne);
                res.TOKEN = authToken;
                yield this.settingRepository.save(res);
                const data = {
                    token: authToken,
                    code: api_error_code_enum_1.ApiErrorCode.SUCCESS,
                    message: '修改成功',
                };
                yield queryRunner.commitTransaction();
                return data;
            }
            catch (err) {
                yield queryRunner.rollbackTransaction();
            }
            finally {
                yield queryRunner.release();
            }
        });
    }
    resetEmail(param) {
        return __awaiter(this, void 0, void 0, function* () {
            const connection = typeorm_2.getConnection();
            const queryRunner = connection.createQueryRunner();
            yield queryRunner.connect();
            yield queryRunner.startTransaction();
            try {
                const res = yield this.settingRepository.findOne({ USER_ID: param.userId });
                const msg = {
                    code: 1,
                    message: '',
                    HttpStatus: 200,
                    data: {},
                };
                if (param.oldEmail.toLowerCase() !== res.EMAIL) {
                    msg.code = api_error_code_enum_1.ApiErrorCode.USER_EMAIL_ERROR;
                    msg.HttpStatus = common_1.HttpStatus.BAD_REQUEST;
                    msg.message = '输入的旧邮箱不正确';
                    return msg;
                }
                if (param.newEmail.toLowerCase() === res.EMAIL) {
                    msg.code = api_error_code_enum_1.ApiErrorCode.USER_EMAIL_ERROR;
                    msg.HttpStatus = common_1.HttpStatus.BAD_REQUEST;
                    msg.message = '不能改为当前绑定邮箱';
                    return msg;
                }
                res.EMAIL = param.newEmail.toLowerCase();
                res.ACTIVITY = false;
                yield this.settingRepository.save(res);
                const data = {
                    code: api_error_code_enum_1.ApiErrorCode.SUCCESS,
                    message: '修改成功, 请前往新邮箱验证',
                };
                yield queryRunner.commitTransaction();
                return data;
            }
            catch (err) {
                yield queryRunner.rollbackTransaction();
            }
            finally {
                yield queryRunner.release();
            }
        });
    }
    validateEmail(param) {
        return __awaiter(this, void 0, void 0, function* () {
            const connection = typeorm_2.getConnection();
            const queryRunner = connection.createQueryRunner();
            yield queryRunner.connect();
            yield queryRunner.startTransaction();
            try {
                let email;
                const res = yield this.settingRepository.findOne({ USER_ID: param.userId });
                const msg = {
                    code: 1,
                    message: '',
                    HttpStatus: 200,
                    data: {},
                };
                if (!param.email || param.email === '') {
                    email = res.EMAIL;
                }
                else {
                    if (param.email !== res.EMAIL) {
                        msg.code = api_error_code_enum_1.ApiErrorCode.USER_EMAIL_ERROR;
                        msg.HttpStatus = common_1.HttpStatus.BAD_REQUEST;
                        msg.message = '与绑定的邮箱不一致！';
                        return msg;
                    }
                    email = param.email;
                }
                const Etoken = jwt.sign({
                    userId: param.userId,
                }, bing_1.util.session.secrets, {
                    expiresIn: '2h',
                });
                send_e_mail_1.Verification.verifica(param.type, param.userId, email, Etoken);
                yield queryRunner.commitTransaction();
                return { code: api_error_code_enum_1.ApiErrorCode.SUCCESS, message: '已发至所绑定邮箱，请前往验证！' };
            }
            catch (err) {
                yield queryRunner.rollbackTransaction();
            }
            finally {
                yield queryRunner.release();
            }
        });
    }
};
SettingService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(user_entity_1.BbsUser)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], SettingService);
exports.SettingService = SettingService;
//# sourceMappingURL=setting.service.js.map