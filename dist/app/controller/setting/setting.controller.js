"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const setting_service_1 = require("./setting.service");
let SettingController = class SettingController {
    constructor(settingService) {
        this.settingService = settingService;
    }
    index(req, res, param) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield this.settingService.getUserInfo(param);
            const tel = data.tel ? data.tel.substr(0, 2) + '****' + data.tel.substr(8, data.tel.split('').length) : '';
            const email = data.email.substr(0, 2) + '****' + data.email.substr(-7);
            const result = {
                email,
                tel,
                activity: data.activity,
            };
            res.render('account/setting', { title: '安全设置', result });
        });
    }
    lose(req, res, param) {
        return __awaiter(this, void 0, void 0, function* () {
            res.render('losePassword/losePassword', { title: '找回密码' });
        });
    }
    resetPassword(params) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.settingService.resetPassword(params);
        });
    }
    resetEmail(params) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.settingService.resetEmail(params);
        });
    }
    validateEmail(params) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log(111111112323, params);
            return yield this.settingService.validateEmail(params);
        });
    }
    getUserInfo(params) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield this.settingService.getUserInfo(params);
            const result = {
                activity: data.activity,
            };
            return result;
        });
    }
};
__decorate([
    common_1.Get('/setting/:userId'),
    swagger_1.ApiOperation({ title: 'get balance from address' }),
    __param(0, common_1.Request()), __param(1, common_1.Response()), __param(2, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], SettingController.prototype, "index", null);
__decorate([
    common_1.Get('/losePassword'),
    swagger_1.ApiOperation({ title: 'get balance from address' }),
    __param(0, common_1.Request()), __param(1, common_1.Response()), __param(2, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], SettingController.prototype, "lose", null);
__decorate([
    common_1.Post('resetPassword'),
    swagger_1.ApiOperation({ title: 'get balance from User' }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], SettingController.prototype, "resetPassword", null);
__decorate([
    common_1.Post('resetEmail'),
    swagger_1.ApiOperation({ title: 'get balance from User' }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], SettingController.prototype, "resetEmail", null);
__decorate([
    common_1.Post('validateEmail'),
    swagger_1.ApiOperation({ title: 'get balance from User' }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], SettingController.prototype, "validateEmail", null);
__decorate([
    common_1.Post('getUserInfo'),
    swagger_1.ApiOperation({ title: 'get balance from User' }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], SettingController.prototype, "getUserInfo", null);
SettingController = __decorate([
    common_1.Controller('account'),
    __metadata("design:paramtypes", [setting_service_1.SettingService])
], SettingController);
exports.SettingController = SettingController;
//# sourceMappingURL=setting.controller.js.map