"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const _ = require('lodash');
const nodemailer = require('nodemailer');
const jwt = require("jsonwebtoken");
const moment = require('moment');
const bing_1 = require("../../../../bing");
const api_error_code_enum_1 = require("../..//../../bing/common/enums/api-error-code.enum");
const common_1 = require("@nestjs/common");
const code = require("../../../code");
const config = {
    host: 'smtp.163.com',
    port: 25,
    auth: {
        user: 'wuzhanfly@163.com',
        pass: 'wzf48690812',
    },
};
const transporter = nodemailer.createTransport(config);
const name = 'Ghost';
const host = 'http://192.168.4.188:3002';
class Verification {
    static verifica(type, userId, email, token) {
        return __awaiter(this, void 0, void 0, function* () {
            const mail = {
                from: 'wuzhanfly@163.com',
                subject: name + '社区',
                to: email,
                html: yield this.PW(type, userId, token, email),
            };
            console.log(333, mail);
            transporter.sendMail(mail, (error, info) => {
                if (error) {
                    return { message: error };
                }
                console.log(info.response);
                return { message: info.response };
            });
        });
    }
    static Everifica(userId, param) {
        return __awaiter(this, void 0, void 0, function* () {
            const msg = {
                code: 1,
                HttpStatus: 200,
                msg: '',
            };
            if (param.token) {
                return yield jwt.verify(param.token, bing_1.util.session.secrets, (err, decoded) => {
                    if (err) {
                        msg.code = api_error_code_enum_1.ApiErrorCode.VERIFICA_EMAIL_Failure;
                        msg.HttpStatus = common_1.HttpStatus.BAD_REQUEST;
                        msg.msg = 'token过期,请前往设置重新验证';
                        console.log(11, msg);
                        return msg;
                    }
                    else {
                        const time = moment(new Date()).unix();
                        if (userId === decoded.userId && time > decoded.iat && time < decoded.exp) {
                            msg.code = api_error_code_enum_1.ApiErrorCode.VERIFICA_EMAIL_SUCCESS;
                            msg.HttpStatus = common_1.HttpStatus.BAD_REQUEST;
                            msg.msg = '验证成功';
                            console.log(11, msg);
                            return msg;
                        }
                        else {
                            msg.code = api_error_code_enum_1.ApiErrorCode.VERIFICA_EMAIL_Failure;
                            msg.HttpStatus = common_1.HttpStatus.BAD_REQUEST;
                            msg.msg = '账号有误,请前往设置重新验证';
                            console.log('user.userId：', userId, 'decoded.userId:', decoded.userId, 'time:', time, 'decoded.exp:', decoded.exp);
                            return msg;
                        }
                    }
                });
            }
            else {
                msg.code = api_error_code_enum_1.ApiErrorCode.VERIFICA_EMAIL_TOKEN;
                msg.HttpStatus = common_1.HttpStatus.BAD_REQUEST;
                msg.msg = 'token有误，请前往设置重新验证';
                console.log(msg);
                return msg;
            }
        });
    }
    static PW(type, userId, token, email) {
        return __awaiter(this, void 0, void 0, function* () {
            if ((type * 1) === 1) {
                return '<p>您好：' + userId + '</p>' +
                    '<p>我们收到您在' + name + '社区的注册信息，请点击下面的链接来激活帐户：</p>' +
                    '<a href="' + host + '/user/verifica/' + userId + '/' + token + '">激活链接</a>' +
                    '<p>若您没有在' + name + '社区填写过注册信息，说明有人滥用了您的电子邮箱，请删除此邮件，我们对给您造成的打扰感到抱歉。</p>' +
                    '<p>' + name + '社区 谨上,　此链接有效期为15min!!!</p>';
            }
            else {
                let info;
                info.time = new Date();
                const No = Math.random().toString().slice(-6);
                code.default.code = No;
                return '<p>您好：' + email + '</p>' +
                    '<p>我们收到您在' + name + '社区的找寻密码信息，请将验证码填入：</p>' +
                    '<p>验证码：' + No + '</p>' +
                    '<p>若您没有在' + name + '社区填写过注册信息，说明有人滥用了您的电子邮箱，请删除此邮件，我们对给您造成的打扰感到抱歉。</p>' +
                    '<p>' + name + '社区 谨上</p>';
            }
        });
    }
}
exports.Verification = Verification;
//# sourceMappingURL=send.e-mail.js.map