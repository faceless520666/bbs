"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const api_error_code_enum_1 = require("../../../bing/common/enums/api-error-code.enum");
const typeorm_2 = require("typeorm");
const user_entity_1 = require("../../entitys/user.entity");
const encrypt_1 = require("../../../bing/common/encrypt");
const send_e_mail_1 = require("./e-mail/send.e-mail");
const jwt = require("jsonwebtoken");
const bing_1 = require("../../../bing");
let RegisterService = class RegisterService {
    constructor(registerRepository) {
        this.registerRepository = registerRepository;
    }
    register(param) {
        return __awaiter(this, void 0, void 0, function* () {
            if (param.passWordOne !== param.passWordTwo) {
                return '两次密码不一致！';
            }
            const paramObj = {
                USER_ID: '',
                PASSWORD: '',
                NICK_NAME: '',
                CREATED: '',
                TOKEN: '',
                EMAIL: '',
            };
            const connection = typeorm_2.getConnection();
            const queryRunner = connection.createQueryRunner();
            yield queryRunner.connect();
            yield queryRunner.startTransaction();
            try {
                const date = new Date().getTime();
                const res = yield this.registerRepository.findOne({ USER_ID: param.userId });
                const resNickName = yield this.registerRepository.findOne({ NICK_NAME: param.nickName });
                const msg = {
                    code: 0,
                    message: '',
                    data: {},
                };
                if (resNickName) {
                    msg.code = api_error_code_enum_1.ApiErrorCode.USER_NAME_HAD;
                    msg.message = '该昵称已占用！';
                    return msg;
                }
                if (res) {
                    msg.code = api_error_code_enum_1.ApiErrorCode.USER_NAME_HAD;
                    msg.message = '该邮箱已被注册！';
                    return msg;
                }
                paramObj.PASSWORD = encrypt_1.md5(param.passWordOne);
                paramObj.NICK_NAME = param.nickName;
                const Etoken = jwt.sign({
                    userId: param.userId,
                }, bing_1.util.session.secrets, {
                    expiresIn: '10min',
                });
                const token = jwt.sign({
                    userId: param.userId,
                }, bing_1.util.session.secrets, {
                    expiresIn: '3d',
                });
                paramObj.USER_ID = param.userId;
                paramObj.CREATED = date + '';
                paramObj.TOKEN = token;
                paramObj.EMAIL = param.userId.toLowerCase();
                yield this.registerRepository.save(paramObj);
                msg.code = api_error_code_enum_1.ApiErrorCode.USER_REGISTER_SUCCESS;
                msg.message = '注册成功';
                const data = {
                    userId: param.userId,
                    nickName: param.nickName,
                    token,
                };
                msg.data = data;
                send_e_mail_1.Verification.verifica(1, param.userId, param.userId, Etoken);
                yield queryRunner.commitTransaction();
                return msg;
            }
            catch (err) {
                yield queryRunner.rollbackTransaction();
            }
            finally {
                yield queryRunner.release();
            }
        });
    }
};
RegisterService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(user_entity_1.BbsUser)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], RegisterService);
exports.RegisterService = RegisterService;
//# sourceMappingURL=register.service.js.map