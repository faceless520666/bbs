import { Repository } from 'typeorm';
import { BbsUser } from '../../entitys/user.entity';
export declare class RegisterService {
    private readonly registerRepository;
    constructor(registerRepository: Repository<BbsUser>);
    register(param: any): Promise<any>;
}
