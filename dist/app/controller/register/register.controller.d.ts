import { RegisterService } from './register.service';
import { BbsUser } from '../../entitys/user.entity';
export declare class RegisterController {
    private readonly registerService;
    constructor(registerService: RegisterService);
    index(req: any, res: any): Promise<any>;
    register(params: any): Promise<BbsUser[]>;
}
