import { AccountService } from './account.service';
export declare class AccountController {
    private readonly accountService;
    constructor(accountService: AccountService);
    index(req: any, res: any, param: any): Promise<any>;
    uploadFile(file: any, data: any): Promise<any>;
    getUserInfo(param: any): Promise<any>;
    changePassWord(req: any, res: any, param: any): Promise<any>;
    changeEmail(req: any, res: any, param: any): Promise<any>;
}
