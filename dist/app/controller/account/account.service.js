"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const api_error_code_enum_1 = require("../../../bing/common/enums/api-error-code.enum");
const typeorm_2 = require("typeorm");
const user_entity_1 = require("../../entitys/user.entity");
const postList_entity_1 = require("../../entitys/postList.entity");
const articleDetail_entity_1 = require("../../entitys/articleDetail.entity");
const commentList_entity_1 = require("../../entitys/commentList.entity");
const childrenComment_entity_1 = require("../../entitys/childrenComment.entity");
const myCollectionList_entity_1 = require("../../entitys/myCollectionList.entity");
let AccountService = class AccountService {
    constructor(accountRepository, postRepository, articleRepository, commentRepository, childCommentRepository, collectRepository) {
        this.accountRepository = accountRepository;
        this.postRepository = postRepository;
        this.articleRepository = articleRepository;
        this.commentRepository = commentRepository;
        this.childCommentRepository = childCommentRepository;
        this.collectRepository = collectRepository;
    }
    getUserInfo(param) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.accountRepository.findOne({ NICK_NAME: param.nickName });
        });
    }
    changeUserInfo(param) {
        return __awaiter(this, void 0, void 0, function* () {
            const msg = {
                code: 200,
                message: '',
            };
            const connection = typeorm_2.getConnection();
            const queryRunner = connection.createQueryRunner();
            yield queryRunner.connect();
            yield queryRunner.startTransaction();
            try {
                const hadNickName = yield this.accountRepository.findOne({ NICK_NAME: param.nickName });
                if (hadNickName && hadNickName.USER_ID !== param.userId) {
                    msg.code = api_error_code_enum_1.ApiErrorCode.CHANGE_USERINFO_FERROR;
                    msg.message = '昵称已被占用，请换另一个！';
                    return msg;
                }
                const res = yield this.accountRepository.findOne({ USER_ID: param.userId });
                if (res) {
                    res.NICK_NAME = param.nickName ? param.nickName : res.NICK_NAME;
                    res.PERSONAL_PROFILE = param.personalProfile ? param.personalProfile : res.PERSONAL_PROFILE;
                    res.HEADER_ICON = param.headerIcon;
                    yield this.accountRepository.save(res);
                    yield this.postRepository
                        .createQueryBuilder()
                        .update()
                        .set({ AUTHOR: res.NICK_NAME })
                        .where('USER_ID = :USER_ID', { USER_ID: res.USER_ID })
                        .execute();
                    yield this.articleRepository
                        .createQueryBuilder()
                        .update()
                        .set({ AUTHOR: res.NICK_NAME })
                        .where('USER_ID = :USER_ID', { USER_ID: res.USER_ID })
                        .execute();
                    yield this.commentRepository
                        .createQueryBuilder()
                        .update()
                        .set({ COMMENTATOR_NAME: res.NICK_NAME })
                        .where('COMMENTATOR_ID = :COMMENTATOR_ID', { COMMENTATOR_ID: res.USER_ID })
                        .execute();
                    yield this.commentRepository
                        .createQueryBuilder()
                        .update()
                        .set({ COMMENTATOR_NAME: res.NICK_NAME })
                        .where('USER_ID = :USER_ID', { USER_ID: res.USER_ID })
                        .execute();
                    yield this.childCommentRepository
                        .createQueryBuilder()
                        .update()
                        .set({
                        COMMENTATOR_NAME: res.NICK_NAME,
                    })
                        .where('COMMENTATOR_ID = :COMMENTATOR_ID', { COMMENTATOR_ID: res.USER_ID })
                        .execute();
                    yield this.childCommentRepository
                        .createQueryBuilder()
                        .update()
                        .set({ COMMENTATOR_NAME: res.NICK_NAME })
                        .where('USER_ID = :USER_ID', { USER_ID: res.USER_ID })
                        .execute();
                    yield this.childCommentRepository
                        .createQueryBuilder()
                        .update()
                        .set({ AUTHOR: res.NICK_NAME })
                        .where('AUTHOR_ID = :AUTHOR_ID', { AUTHOR_ID: res.USER_ID })
                        .execute();
                    yield this.collectRepository
                        .createQueryBuilder()
                        .update()
                        .set({ AUTHOR: res.NICK_NAME })
                        .where('AUTHOR_ID = :AUTHOR_ID', { AUTHOR_ID: res.USER_ID })
                        .execute();
                    msg.code = api_error_code_enum_1.ApiErrorCode.SUCCESS;
                    msg.message = '修改成功！';
                }
                else {
                    msg.code = api_error_code_enum_1.ApiErrorCode.CHANGE_USERINFO_FERROR;
                    msg.message = '修改失败！';
                }
                yield queryRunner.commitTransaction();
                return msg;
            }
            catch (err) {
                yield queryRunner.rollbackTransaction();
            }
            finally {
                yield queryRunner.release();
            }
        });
    }
};
AccountService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(user_entity_1.BbsUser)),
    __param(1, typeorm_1.InjectRepository(postList_entity_1.BbsPostList)),
    __param(2, typeorm_1.InjectRepository(articleDetail_entity_1.BbsArticleDetail)),
    __param(3, typeorm_1.InjectRepository(commentList_entity_1.BbsCommentsList)),
    __param(4, typeorm_1.InjectRepository(childrenComment_entity_1.BbsChildrenComments)),
    __param(5, typeorm_1.InjectRepository(myCollectionList_entity_1.BbsMyCollectionList)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository])
], AccountService);
exports.AccountService = AccountService;
//# sourceMappingURL=account.service.js.map