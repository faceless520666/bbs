import { Repository } from 'typeorm';
import { BbsUser } from '../../entitys/user.entity';
import { BbsPostList } from '../../entitys/postList.entity';
import { BbsArticleDetail } from '../../entitys/articleDetail.entity';
import { BbsCommentsList } from '../../entitys/commentList.entity';
import { BbsChildrenComments } from '../../entitys/childrenComment.entity';
import { BbsMyCollectionList } from '../../entitys/myCollectionList.entity';
export declare class AccountService {
    private readonly accountRepository;
    private readonly postRepository;
    private readonly articleRepository;
    private readonly commentRepository;
    private readonly childCommentRepository;
    private readonly collectRepository;
    constructor(accountRepository: Repository<BbsUser>, postRepository: Repository<BbsPostList>, articleRepository: Repository<BbsArticleDetail>, commentRepository: Repository<BbsCommentsList>, childCommentRepository: Repository<BbsChildrenComments>, collectRepository: Repository<BbsMyCollectionList>);
    getUserInfo(param: any): Promise<any>;
    changeUserInfo(param: any): Promise<any>;
}
