"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const account_service_1 = require("./account.service");
const bing_1 = require("../../../bing");
const multer_1 = require("multer");
const path_1 = require("path");
let AccountController = class AccountController {
    constructor(accountService) {
        this.accountService = accountService;
    }
    index(req, res, param) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield this.accountService.getUserInfo(param);
            const tel = data.TEL ? data.TEL.substr(0, 2) + '****' + data.TEL.substr(8, data.TEL.split('').length) : '';
            const email = data.EMAIL.substr(0, 2) + '****' + data.EMAIL.substr(-7);
            const headerIcon = data.HEADER_ICON;
            const nickName = data.NICK_NAME;
            const personalProfile = data.PERSONAL_PROFILE;
            const result = {
                email,
                tel,
                headerIcon,
                nickName,
                personalProfile,
                activity: data.ACTIVITY,
            };
            res.render('account/personalCenter/account', { title: '个人中心', result });
        });
    }
    uploadFile(file, data) {
        return __awaiter(this, void 0, void 0, function* () {
            const params = {
                userId: data.userId,
                nickName: data.nickName,
                personalProfile: data.personalProfile,
                headerIcon: '',
            };
            try {
                if (file) {
                    return bing_1.util.client.write(file.path)
                        .then((fileInfo) => __awaiter(this, void 0, void 0, function* () {
                        params.headerIcon = fileInfo.fid;
                        return yield this.accountService.changeUserInfo(params);
                    }));
                }
            }
            catch (e) {
                console.log(e);
            }
        });
    }
    getUserInfo(param) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield this.accountService.getUserInfo(param);
            let msg = {
                code: 1,
                activity: -1,
                hadNews: false,
                headerIcon: '',
            };
            if (data) {
                msg = {
                    code: 10000,
                    headerIcon: data.HEADER_ICON,
                    hadNews: data.HAD_NEW,
                    activity: data.ACTIVITY,
                };
            }
            else {
                msg = {
                    code: -1,
                    headerIcon: '',
                    hadNews: false,
                    activity: -1,
                };
            }
            return msg;
        });
    }
    changePassWord(req, res, param) {
        return __awaiter(this, void 0, void 0, function* () {
            res.render('account/appChangePw/appChangePw', { title: '修改密码' });
        });
    }
    changeEmail(req, res, param) {
        return __awaiter(this, void 0, void 0, function* () {
            res.render('account/appChangeEmail/appChangeEmail', { title: '修改邮箱' });
        });
    }
};
__decorate([
    common_1.Get('/account/:nickName'),
    swagger_1.ApiOperation({ title: 'get balance from user' }),
    __param(0, common_1.Request()), __param(1, common_1.Response()), __param(2, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], AccountController.prototype, "index", null);
__decorate([
    common_1.Post('upload'),
    common_1.UseInterceptors(common_1.FileInterceptor('file', {
        storage: multer_1.diskStorage({
            filename: (req, file, cb) => {
                const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('');
                return cb(null, `${randomName}${path_1.extname(file.originalname)}`);
            },
        }),
    })),
    __param(0, common_1.UploadedFile()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], AccountController.prototype, "uploadFile", null);
__decorate([
    common_1.Post('getUserInfo'),
    swagger_1.ApiOperation({ title: 'get balance from user' }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AccountController.prototype, "getUserInfo", null);
__decorate([
    common_1.Get('user/changePassWord'),
    swagger_1.ApiOperation({ title: 'get balance from user' }),
    __param(0, common_1.Request()), __param(1, common_1.Response()), __param(2, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], AccountController.prototype, "changePassWord", null);
__decorate([
    common_1.Get('user/changeEmail'),
    swagger_1.ApiOperation({ title: 'get balance from user' }),
    __param(0, common_1.Request()), __param(1, common_1.Response()), __param(2, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], AccountController.prototype, "changeEmail", null);
AccountController = __decorate([
    common_1.Controller('account'),
    __metadata("design:paramtypes", [account_service_1.AccountService])
], AccountController);
exports.AccountController = AccountController;
//# sourceMappingURL=account.controller.js.map