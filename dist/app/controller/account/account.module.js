"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const account_service_1 = require("./account.service");
const account_controller_1 = require("./account.controller");
const user_entity_1 = require("../../entitys/user.entity");
const postList_entity_1 = require("../../entitys/postList.entity");
const articleDetail_entity_1 = require("../../entitys/articleDetail.entity");
const commentList_entity_1 = require("../../entitys/commentList.entity");
const childrenComment_entity_1 = require("../../entitys/childrenComment.entity");
const myCollectionList_entity_1 = require("../../entitys/myCollectionList.entity");
let AccountModule = class AccountModule {
};
AccountModule = __decorate([
    common_1.Module({
        imports: [typeorm_1.TypeOrmModule.forFeature([user_entity_1.BbsUser, postList_entity_1.BbsPostList, articleDetail_entity_1.BbsArticleDetail, commentList_entity_1.BbsCommentsList, childrenComment_entity_1.BbsChildrenComments, myCollectionList_entity_1.BbsMyCollectionList])],
        providers: [account_service_1.AccountService],
        controllers: [account_controller_1.AccountController],
    })
], AccountModule);
exports.AccountModule = AccountModule;
//# sourceMappingURL=account.module.js.map