import { UpdateService } from './update.service';
export declare class UpdateController {
    private readonly loginService;
    constructor(loginService: UpdateService);
    index(req: any, res: any): Promise<any>;
    download(req: any, res: any): Promise<any>;
}
