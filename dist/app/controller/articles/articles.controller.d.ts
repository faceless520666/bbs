import { ArticlesService } from './articles.service';
import { BbsUser } from '../../entitys/user.entity';
export declare class ArticlesController {
    private readonly focusService;
    constructor(focusService: ArticlesService);
    index(req: any, res: any): Promise<any>;
    register(params: any): Promise<BbsUser[]>;
}
