"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const api_error_code_enum_1 = require("../../../bing/common/enums/api-error-code.enum");
const typeorm_2 = require("typeorm");
const postList_entity_1 = require("../../entitys/postList.entity");
const user_entity_1 = require("../../entitys/user.entity");
let SearchPageService = class SearchPageService {
    constructor(postListRepository, userRepository) {
        this.postListRepository = postListRepository;
        this.userRepository = userRepository;
    }
    searchArticle(param) {
        return __awaiter(this, void 0, void 0, function* () {
            let res;
            let totalRes;
            const pageCount = param.pageCount ? param.pageCount * 1 : 10;
            const page = param.page ? param.page * 1 * pageCount : 0;
            totalRes = yield this.postListRepository
                .createQueryBuilder('posts')
                .where('posts.AUTHOR LIKE :param')
                .orWhere('posts.ARTICLE_TITLE LIKE :param')
                .andWhere('posts.IS_DRAFTS = :IS_DRAFTS', { IS_DRAFTS: false })
                .setParameters({
                param: '%' + param.keyword + '%',
            })
                .getMany();
            res = yield this.postListRepository
                .createQueryBuilder('posts')
                .where('posts.AUTHOR LIKE :param')
                .orWhere('posts.ARTICLE_TITLE LIKE :param')
                .andWhere('posts.IS_DRAFTS = :IS_DRAFTS', { IS_DRAFTS: false })
                .setParameters({
                param: '%' + param.keyword + '%',
            })
                .skip(page)
                .take(pageCount)
                .getMany();
            if (res.length > 0) {
                for (const item of res) {
                    const user = yield this.userRepository.findOne({ USER_ID: item.USER_ID });
                    item.hearderIcon = user.HEADER_ICON;
                    item.author = user.NICK_NAME;
                }
            }
            const msg = {
                code: 1,
                message: '',
                HttpStatus: 200,
                total: 0,
                data: {},
            };
            msg.HttpStatus = common_1.HttpStatus.BAD_REQUEST;
            if (res) {
                msg.code = api_error_code_enum_1.ApiErrorCode.SUCCESS;
                msg.message = '查询成功！';
                msg.data = res;
                msg.total = totalRes.length;
                return msg;
            }
            else {
                msg.code = api_error_code_enum_1.ApiErrorCode.NO_DATA;
                msg.message = '查无数据！';
                return msg;
            }
        });
    }
};
SearchPageService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(postList_entity_1.BbsPostList)),
    __param(1, typeorm_1.InjectRepository(user_entity_1.BbsUser)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository])
], SearchPageService);
exports.SearchPageService = SearchPageService;
//# sourceMappingURL=searchPage.service.js.map