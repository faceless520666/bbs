import { SearchPageService } from './searchPage.service';
export declare class SearchPageController {
    private readonly searchRepository;
    constructor(searchRepository: SearchPageService);
    index(req: any, res: any, keyword: any): Promise<any>;
    getPage(keyword: any): Promise<any>;
}
