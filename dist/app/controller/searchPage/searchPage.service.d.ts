import { Repository } from 'typeorm';
import { BbsPostList } from '../../entitys/postList.entity';
import { BbsUser } from '../../entitys/user.entity';
export declare class SearchPageService {
    private readonly postListRepository;
    private readonly userRepository;
    constructor(postListRepository: Repository<BbsPostList>, userRepository: Repository<BbsUser>);
    searchArticle(param: any): Promise<any>;
}
