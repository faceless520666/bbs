"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const articleDetail_service_1 = require("./articleDetail.service");
const curUserId = require("../../global");
let ArticleDetailController = class ArticleDetailController {
    constructor(postsRepository) {
        this.postsRepository = postsRepository;
    }
    index(req, res, param) {
        return __awaiter(this, void 0, void 0, function* () {
            const article = yield this.postsRepository.getArticle(param);
            article.ARTICLE_LABEL = article.ARTICLE_LABEL.split(',');
            res.render('articleDetail/articleDetail', { title: 'articleDetail', article });
        });
    }
    getComment(param) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.postsRepository.getArticle(param);
        });
    }
    preview(req, res, param) {
        return __awaiter(this, void 0, void 0, function* () {
            const article = yield this.postsRepository.getArticle(param);
            article.ARTICLE_LABEL = article.ARTICLE_LABEL.split(',');
            res.render('preview/preview', { title: 'preview', article });
        });
    }
    addComment(param) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.postsRepository.addComment(param);
        });
    }
    addChildrenComment(param) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.postsRepository.addChildrenComment(param);
        });
    }
    deleteComment(param) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.postsRepository.deleteComment(param);
        });
    }
    addLike(param) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.postsRepository.addLike(param);
        });
    }
    addCollect(param) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.postsRepository.addCollect(param);
        });
    }
    addView(param) {
        return __awaiter(this, void 0, void 0, function* () {
            curUserId.default.userId = param.userId;
            return this.postsRepository.addView(param);
        });
    }
};
__decorate([
    common_1.Get('/articleDetail/:articleId'),
    swagger_1.ApiOperation({ title: 'get balance from articleDetail' }),
    __param(0, common_1.Request()), __param(1, common_1.Response()), __param(2, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], ArticleDetailController.prototype, "index", null);
__decorate([
    common_1.Get('/articleDetail/:articleId/:page'),
    swagger_1.ApiOperation({ title: 'get balance from commentList' }),
    __param(0, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ArticleDetailController.prototype, "getComment", null);
__decorate([
    common_1.Get('/preview/:articleId'),
    swagger_1.ApiOperation({ title: 'get balance from postList' }),
    __param(0, common_1.Request()), __param(1, common_1.Response()), __param(2, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], ArticleDetailController.prototype, "preview", null);
__decorate([
    common_1.Post('addComment'),
    swagger_1.ApiOperation({ title: 'get balance from commentList' }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ArticleDetailController.prototype, "addComment", null);
__decorate([
    common_1.Post('addChildrenComment'),
    swagger_1.ApiOperation({ title: 'get balance from childrenCommentList' }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ArticleDetailController.prototype, "addChildrenComment", null);
__decorate([
    common_1.Post('deleteComment'),
    swagger_1.ApiOperation({ title: 'get balance from childrenCommentList' }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ArticleDetailController.prototype, "deleteComment", null);
__decorate([
    common_1.Post('addLike'),
    swagger_1.ApiOperation({ title: 'get balance from postList' }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ArticleDetailController.prototype, "addLike", null);
__decorate([
    common_1.Post('addCollect'),
    swagger_1.ApiOperation({ title: 'get balance from postList' }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ArticleDetailController.prototype, "addCollect", null);
__decorate([
    common_1.Post('addView'),
    swagger_1.ApiOperation({ title: 'get balance from postList' }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ArticleDetailController.prototype, "addView", null);
ArticleDetailController = __decorate([
    swagger_1.ApiUseTags('post'),
    common_1.Controller('post'),
    __metadata("design:paramtypes", [articleDetail_service_1.ArticleDetailService])
], ArticleDetailController);
exports.ArticleDetailController = ArticleDetailController;
//# sourceMappingURL=articleDetail.controller.js.map