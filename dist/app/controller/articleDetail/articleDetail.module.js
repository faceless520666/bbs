"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const articleDetail_service_1 = require("./articleDetail.service");
const articleDetail_controller_1 = require("./articleDetail.controller");
const articleDetail_entity_1 = require("../../entitys/articleDetail.entity");
const myCollectionList_entity_1 = require("../../entitys/myCollectionList.entity");
const commentList_entity_1 = require("../../entitys/commentList.entity");
const childrenComment_entity_1 = require("../../entitys/childrenComment.entity");
const myLikeList_entity_1 = require("../../entitys/myLikeList.entity");
const postList_entity_1 = require("../../entitys/postList.entity");
const user_entity_1 = require("../../entitys/user.entity");
let ArticleDetailModule = class ArticleDetailModule {
};
ArticleDetailModule = __decorate([
    common_1.Module({
        imports: [typeorm_1.TypeOrmModule.forFeature([articleDetail_entity_1.BbsArticleDetail, myCollectionList_entity_1.BbsMyCollectionList, myLikeList_entity_1.BbsMyLikeList, commentList_entity_1.BbsCommentsList, childrenComment_entity_1.BbsChildrenComments, postList_entity_1.BbsPostList, user_entity_1.BbsUser])],
        providers: [articleDetail_service_1.ArticleDetailService],
        controllers: [articleDetail_controller_1.ArticleDetailController],
    })
], ArticleDetailModule);
exports.ArticleDetailModule = ArticleDetailModule;
//# sourceMappingURL=articleDetail.module.js.map