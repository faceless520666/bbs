import { ArticleDetailService } from './articleDetail.service';
import { BbsCommentsList } from '../../entitys/commentList.entity';
import { BbsPostList } from '../../entitys/postList.entity';
export declare class ArticleDetailController {
    private readonly postsRepository;
    constructor(postsRepository: ArticleDetailService);
    index(req: any, res: any, param: any): Promise<any>;
    getComment(param: any): Promise<BbsCommentsList[]>;
    preview(req: any, res: any, param: any): Promise<any>;
    addComment(param: any): Promise<BbsCommentsList[]>;
    addChildrenComment(param: any): Promise<BbsCommentsList[]>;
    deleteComment(param: any): Promise<BbsCommentsList[]>;
    addLike(param: any): Promise<BbsPostList[]>;
    addCollect(param: any): Promise<BbsPostList[]>;
    addView(param: any): Promise<BbsPostList[]>;
}
