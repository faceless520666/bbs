import { Repository } from 'typeorm';
import { BbsArticleDetail } from '../../entitys/articleDetail.entity';
import { BbsCommentsList } from '../../entitys/commentList.entity';
import { BbsChildrenComments } from '../../entitys/childrenComment.entity';
import { BbsMyCollectionList } from '../../entitys/myCollectionList.entity';
import { BbsMyLikeList } from '../../entitys/myLikeList.entity';
import { BbsPostList } from '../../entitys/postList.entity';
import { BbsUser } from '../../entitys/user.entity';
export declare class ArticleDetailService {
    private readonly postListRepository;
    private readonly articleRepository;
    private readonly myCollectRepository;
    private readonly myLikeRepository;
    private readonly commentRepository;
    private readonly childrenCommentRepository;
    private readonly userRepository;
    constructor(postListRepository: Repository<BbsPostList>, articleRepository: Repository<BbsArticleDetail>, myCollectRepository: Repository<BbsMyCollectionList>, myLikeRepository: Repository<BbsMyLikeList>, commentRepository: Repository<BbsCommentsList>, childrenCommentRepository: Repository<BbsChildrenComments>, userRepository: Repository<BbsUser>);
    getArticle(param: any): Promise<any>;
    addComment(param: any): Promise<any>;
    addChildrenComment(param: any): Promise<any>;
    deleteComment(param: any): Promise<any>;
    addLike(param: any): Promise<any>;
    addCollect(param: any): Promise<any>;
    addView(param: any): Promise<any>;
}
