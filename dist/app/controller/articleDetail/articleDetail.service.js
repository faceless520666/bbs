"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const api_error_code_enum_1 = require("../../../bing/common/enums/api-error-code.enum");
const typeorm_2 = require("typeorm");
const articleDetail_entity_1 = require("../../entitys/articleDetail.entity");
const commentList_entity_1 = require("../../entitys/commentList.entity");
const childrenComment_entity_1 = require("../../entitys/childrenComment.entity");
const myCollectionList_entity_1 = require("../../entitys/myCollectionList.entity");
const myLikeList_entity_1 = require("../../entitys/myLikeList.entity");
const postList_entity_1 = require("../../entitys/postList.entity");
const user_entity_1 = require("../../entitys/user.entity");
const bing_1 = require("../../../bing");
const curUserId = require("../../global");
let ArticleDetailService = class ArticleDetailService {
    constructor(postListRepository, articleRepository, myCollectRepository, myLikeRepository, commentRepository, childrenCommentRepository, userRepository) {
        this.postListRepository = postListRepository;
        this.articleRepository = articleRepository;
        this.myCollectRepository = myCollectRepository;
        this.myLikeRepository = myLikeRepository;
        this.commentRepository = commentRepository;
        this.childrenCommentRepository = childrenCommentRepository;
        this.userRepository = userRepository;
    }
    getArticle(param) {
        return __awaiter(this, void 0, void 0, function* () {
            let commentRes;
            let commentNum = 0;
            const pageCount = param.pageCount ? param.pageCount * 1 : 10;
            const page = param.page ? param.page * 1 * pageCount : 0;
            const totalRes = yield this.commentRepository.find({ ARTICLE_ID: param.articleId });
            const articleRes = yield this.articleRepository.findOne({ ARTICLE_ID: param.articleId });
            const userInfoRes = yield this.userRepository.findOne({ USER_ID: articleRes.USER_ID });
            commentRes = yield this.commentRepository
                .createQueryBuilder('commentleList')
                .where('commentleList.ARTICLE_ID = :ARTICLE_ID', { ARTICLE_ID: param.articleId })
                .skip(page)
                .take(pageCount)
                .getMany();
            const collectRes = yield this.myCollectRepository.findOne({ ARTICLE_ID: param.articleId, USER_ID: curUserId.default.userId });
            if (commentRes.length > 0) {
                for (const item of commentRes) {
                    const user = yield this.userRepository.findOne({ USER_ID: item.USER_ID });
                    item.hearderIcon = user.HEADER_ICON;
                    const childrenComRes = yield this.childrenCommentRepository.find({ ARTICLE_ID: param.articleId, COMMENT_ID: item.COMMENT_ID });
                    item.childrenComentList = childrenComRes;
                    commentNum += childrenComRes.length * 1;
                }
            }
            const likeRes = yield this.myLikeRepository.findOne({ ARTICLE_ID: param.articleId, USER_ID: curUserId.default.userId });
            articleRes.comments = commentRes;
            articleRes.IS_COLLECT = collectRes ? true : false;
            const resObj = Object.assign(articleRes);
            resObj.personalProfile = userInfoRes.PERSONAL_PROFILE;
            resObj.total = totalRes.length;
            resObj.headerIcon = userInfoRes.HEADER_ICON;
            resObj.commentTotal = commentNum + commentRes.length;
            resObj.IS_LIKE = likeRes ? true : false;
            delete resObj.userId;
            return resObj;
        });
    }
    addComment(param) {
        return __awaiter(this, void 0, void 0, function* () {
            const connection = typeorm_2.getConnection();
            const queryRunner = connection.createQueryRunner();
            yield queryRunner.connect();
            yield queryRunner.startTransaction();
            try {
                const dateNum = new Date().getTime().toString().substring(-10);
                const user = yield this.userRepository.findOne({ NICK_NAME: param.commentatorName });
                if (user.USER_ID !== param.userId) {
                    user.HAD_NEWS = true;
                    this.userRepository.save(user);
                }
                const comment = new commentList_entity_1.BbsCommentsList();
                comment.USER_ID = param.userId;
                comment.COMMENTATOR_ID = user.USER_ID;
                comment.COMMENTATOR_NAME = param.commentatorName;
                comment.ARTICLE_TITLE = param.articleTitle;
                comment.ARTICLE_ID = param.articleId;
                comment.COMMENT_CONTENT = param.commentText;
                comment.CREATED = (yield bing_1.util.dateType.getTime()) + '';
                comment.COMMENT_ID = dateNum + bing_1.util.ramdom.random(6);
                comment.COMMENT_USER_NAME = param.nickName;
                const updataPosts = yield this.postListRepository.findOne({ ARTICLE_ID: param.articleId });
                const updataArticle = yield this.articleRepository.findOne({ ARTICLE_ID: param.articleId });
                const updataCollectRes = yield this.myCollectRepository.findOne({ ARTICLE_ID: param.articleId, USER_ID: param.userId });
                updataArticle.COMMENT_ID = comment.COMMENT_ID;
                updataArticle.COMMENT_COUNT += 1;
                updataPosts.COMMENT_COUNT = updataArticle.COMMENT_COUNT;
                if (updataCollectRes) {
                    updataCollectRes.COMMENT_COUNT = updataArticle.COMMENT_COUNT;
                    yield this.myCollectRepository.save(updataCollectRes);
                }
                yield this.postListRepository.save(updataPosts);
                yield this.articleRepository.save(updataArticle);
                yield this.articleRepository.manager.save(comment);
                const msg = {
                    code: api_error_code_enum_1.ApiErrorCode.SUCCESS,
                    commentId: comment.COMMENT_ID,
                    message: '评论成功！',
                };
                yield queryRunner.commitTransaction();
                return msg;
            }
            catch (err) {
                yield queryRunner.rollbackTransaction();
            }
            finally {
                yield queryRunner.release();
            }
        });
    }
    addChildrenComment(param) {
        return __awaiter(this, void 0, void 0, function* () {
            const connection = typeorm_2.getConnection();
            const queryRunner = connection.createQueryRunner();
            yield queryRunner.connect();
            yield queryRunner.startTransaction();
            try {
                const user = yield this.userRepository.findOne({ NICK_NAME: param.author });
                const commentatorUserInfo = yield this.userRepository.findOne({ NICK_NAME: param.commentatorName });
                const dateNum = new Date().getTime().toString().substring(-11);
                const childrenCom = new childrenComment_entity_1.BbsChildrenComments();
                if (user.USER_ID !== param.userId) {
                    user.HAD_NEWS = true;
                    this.userRepository.save(user);
                }
                childrenCom.USER_ID = param.userId;
                childrenCom.COMMENT_USER_NAME = param.nickName;
                childrenCom.AUTHOR = param.author;
                childrenCom.AUTHOR_ID = user.USER_ID;
                childrenCom.COMMENTATOR_ID = commentatorUserInfo.USER_ID;
                childrenCom.COMMENTATOR_NAME = param.commentatorName;
                childrenCom.ARTICLE_ID = param.articleId;
                childrenCom.COMMENT_CONTENT = param.commentText;
                childrenCom.CREATED = (yield bing_1.util.dateType.getTime()) + '';
                childrenCom.CHILD_COMMENT_ID = dateNum + bing_1.util.ramdom.random(6);
                childrenCom.COMMENT_ID = param.commentId;
                const updataPosts = yield this.postListRepository.findOne({ ARTICLE_ID: param.articleId });
                const updataArticle = yield this.articleRepository.findOne({ ARTICLE_ID: param.articleId });
                const updataCollectRes = yield this.myCollectRepository.findOne({ ARTICLE_ID: param.articleId, USER_ID: param.userId });
                updataArticle.COMMENT_COUNT += 1;
                updataPosts.COMMENT_COUNT = updataArticle.COMMENT_COUNT;
                if (updataCollectRes) {
                    updataCollectRes.COMMENT_COUNT = updataArticle.COMMENT_COUNT;
                    yield this.myCollectRepository.save(updataCollectRes);
                }
                yield this.postListRepository.save(updataPosts);
                yield this.articleRepository.save(updataArticle);
                yield this.articleRepository.manager.save(childrenCom);
                const msg = {
                    code: api_error_code_enum_1.ApiErrorCode.SUCCESS,
                    commentId: childrenCom.CHILD_COMMENT_ID,
                    message: '回复成功！',
                };
                yield queryRunner.commitTransaction();
                return msg;
            }
            catch (err) {
                yield queryRunner.rollbackTransaction();
            }
            finally {
                yield queryRunner.release();
            }
        });
    }
    deleteComment(param) {
        return __awaiter(this, void 0, void 0, function* () {
            const msg = {
                code: 0,
                HttpStatus: 200,
                message: '',
            };
            const connection = typeorm_2.getConnection();
            const queryRunner = connection.createQueryRunner();
            yield queryRunner.connect();
            yield queryRunner.startTransaction();
            try {
                if (param.commentType.toLowerCase() === 'parent') {
                    const parComment = yield this.commentRepository.findOne({ ARTICLE_ID: param.articleId, COMMENT_ID: param.commentId });
                    const childComment = yield this.childrenCommentRepository.find({ ARTICLE_ID: param.articleId, COMMENT_ID: param.commentId });
                    if (parComment) {
                        yield this.commentRepository.remove(parComment);
                        if (childComment.length > 0) {
                            yield this.childrenCommentRepository.remove(childComment);
                        }
                        msg.code = api_error_code_enum_1.ApiErrorCode.DELETE_SUCCESS;
                        msg.message = '删除成功！';
                    }
                    else {
                        msg.code = api_error_code_enum_1.ApiErrorCode.REMOVE_FAILT;
                        msg.HttpStatus = common_1.HttpStatus.BAD_REQUEST;
                        msg.message = '删除失败！';
                    }
                }
                else {
                    const delChild = yield this.childrenCommentRepository.findOne({ ARTICLE_ID: param.articleId, CHILD_COMMENT_ID: param.commentId });
                    if (delChild) {
                        yield this.childrenCommentRepository.remove(delChild);
                        msg.code = api_error_code_enum_1.ApiErrorCode.DELETE_SUCCESS;
                        msg.message = '删除成功！';
                    }
                    else {
                        msg.code = api_error_code_enum_1.ApiErrorCode.REMOVE_FAILT;
                        msg.HttpStatus = common_1.HttpStatus.BAD_REQUEST;
                        msg.message = '删除失败！';
                    }
                }
                let commentNum = 0;
                const commentRes = yield this.commentRepository.find({ ARTICLE_ID: param.articleId });
                if (commentRes.length > 0) {
                    for (const item of commentRes) {
                        const childrenComRes = yield this.childrenCommentRepository.find({ ARTICLE_ID: param.articleId, COMMENT_ID: item.COMMENT_ID });
                        commentNum += childrenComRes.length * 1;
                    }
                }
                const updataPosts = yield this.postListRepository.findOne({ ARTICLE_ID: param.articleId });
                const updataArticle = yield this.articleRepository.findOne({ ARTICLE_ID: param.articleId });
                const updataCollectRes = yield this.myCollectRepository.findOne({ ARTICLE_ID: param.articleId, USER_ID: param.userId });
                updataArticle.COMMENT_COUNT = commentRes.length + commentNum;
                updataPosts.COMMENT_COUNT = updataArticle.COMMENT_COUNT;
                if (updataCollectRes) {
                    updataCollectRes.COMMENT_COUNT = updataArticle.COMMENT_COUNT;
                    yield this.myCollectRepository.save(updataCollectRes);
                }
                yield this.postListRepository.save(updataPosts);
                yield this.articleRepository.save(updataArticle);
                yield queryRunner.commitTransaction();
                return msg;
            }
            catch (err) {
                yield queryRunner.rollbackTransaction();
            }
            finally {
                yield queryRunner.release();
            }
        });
    }
    addLike(param) {
        return __awaiter(this, void 0, void 0, function* () {
            const connection = typeorm_2.getConnection();
            const queryRunner = connection.createQueryRunner();
            yield queryRunner.connect();
            yield queryRunner.startTransaction();
            try {
                const articleRes = yield this.articleRepository.findOne({ ARTICLE_ID: param.articleId });
                const postRes = yield this.postListRepository.findOne({ ARTICLE_ID: param.articleId });
                const collectRes = yield this.myCollectRepository.findOne({ ARTICLE_ID: param.articleId, USER_ID: param.userId });
                const likeRes = yield this.myLikeRepository.findOne({ ARTICLE_ID: param.articleId, USER_ID: param.userId });
                const user = yield this.userRepository.findOne({ NICK_NAME: param.author });
                if (!likeRes) {
                    articleRes.LIKE_COUNT = articleRes.LIKE_COUNT + 1;
                    postRes.LIKE_COUNT = articleRes.LIKE_COUNT;
                    yield this.articleRepository.save(articleRes);
                    yield this.postListRepository.save(postRes);
                    const myLike = new myLikeList_entity_1.BbsMyLikeList();
                    myLike.USER_ID = param.userId;
                    myLike.ARTICLE_ID = param.articleId;
                    myLike.LIKE_COUNT = articleRes.LIKE_COUNT;
                    myLike.IS_LIKE = true;
                    myLike.CREATED = (yield bing_1.util.dateType.getTime()) + '';
                    yield this.myLikeRepository.save(myLike);
                    if (collectRes) {
                        collectRes.LIKE_COUNT = articleRes.LIKE_COUNT;
                        yield this.myCollectRepository.save(collectRes);
                    }
                    yield queryRunner.commitTransaction();
                    return { code: api_error_code_enum_1.ApiErrorCode.SUCCESS, isLike: myLike.IS_LIKE, message: '已标记为喜欢！' };
                }
                else {
                    articleRes.LIKE_COUNT = (articleRes.LIKE_COUNT - 1) > 0 ? articleRes.LIKE_COUNT - 1 : 0;
                    postRes.LIKE_COUNT = articleRes.LIKE_COUNT;
                    if (collectRes) {
                        collectRes.LIKE_COUNT = articleRes.LIKE_COUNT;
                        yield this.myCollectRepository.save(collectRes);
                    }
                    yield this.myLikeRepository.delete({ ARTICLE_ID: param.articleId, USER_ID: param.userId });
                    yield this.articleRepository.save(articleRes);
                    yield this.postListRepository.save(postRes);
                    yield queryRunner.commitTransaction();
                    return { code: api_error_code_enum_1.ApiErrorCode.SUCCESS, isLike: false, message: '已移出喜欢！' };
                }
            }
            catch (err) {
                yield queryRunner.rollbackTransaction();
            }
            finally {
                yield queryRunner.release();
            }
        });
    }
    addCollect(param) {
        return __awaiter(this, void 0, void 0, function* () {
            const connection = typeorm_2.getConnection();
            const queryRunner = connection.createQueryRunner();
            yield queryRunner.connect();
            yield queryRunner.startTransaction();
            try {
                const articleRes = yield this.articleRepository.findOne({ ARTICLE_ID: param.articleId });
                const postRes = yield this.postListRepository.findOne({ ARTICLE_ID: param.articleId });
                const collectRes = yield this.myCollectRepository.findOne({ ARTICLE_ID: param.articleId, USER_ID: param.userId });
                const user = yield this.userRepository.findOne({ NICK_NAME: param.author });
                console.log(111, collectRes, articleRes);
                if (!collectRes) {
                    const myCollection = new myCollectionList_entity_1.BbsMyCollectionList();
                    articleRes.COLLECT_COUNT = articleRes.COLLECT_COUNT + 1;
                    postRes.COLLECT_COUNT = postRes.COLLECT_COUNT + 1;
                    yield this.articleRepository.save(articleRes);
                    console.log(444, myCollection);
                    yield this.postListRepository.save(postRes);
                    const paramObj = Object.assign(myCollection, articleRes);
                    console.log(333, paramObj);
                    paramObj.USER_ID = param.userId;
                    paramObj.AUTHOR_ID = user.USER_ID;
                    paramObj.IS_COLLECT = true;
                    paramObj.ARTICLE_CONTENT = postRes.ARTICLE_CONTENT;
                    paramObj.CREATED = (yield bing_1.util.dateType.getTime()) + '';
                    delete paramObj.ID;
                    console.log(222);
                    yield this.myCollectRepository.save(paramObj);
                    yield queryRunner.commitTransaction();
                    return { code: api_error_code_enum_1.ApiErrorCode.SUCCESS, isCollect: true, message: '收藏成功！' };
                }
                else {
                    articleRes.COLLECT_COUNT = (articleRes.COLLECT_COUNT - 1) > 0 ? articleRes.COLLECT_COUNT - 1 : 0;
                    postRes.COLLECT_COUNT = articleRes.COLLECT_COUNT;
                    yield this.myCollectRepository.delete({ ARTICLE_ID: param.articleId, USER_ID: param.userId });
                    yield this.articleRepository.save(articleRes);
                    yield this.postListRepository.save(postRes);
                    yield queryRunner.commitTransaction();
                    return { code: api_error_code_enum_1.ApiErrorCode.SUCCESS, isCollect: false, message: '取消成功！' };
                }
            }
            catch (err) {
                yield queryRunner.rollbackTransaction();
            }
            finally {
                yield queryRunner.release();
            }
        });
    }
    addView(param) {
        return __awaiter(this, void 0, void 0, function* () {
            const connection = typeorm_2.getConnection();
            const queryRunner = connection.createQueryRunner();
            yield queryRunner.connect();
            yield queryRunner.startTransaction();
            try {
                const res = yield this.articleRepository.findOne({ ARTICLE_ID: param.articleId });
                const postRes = yield this.articleRepository.findOne({ ARTICLE_ID: param.articleId });
                const myCollectRes = yield this.myCollectRepository.findOne({ ARTICLE_ID: param.articleId });
                res.VIEW_COUNT = res.VIEW_COUNT + 1;
                postRes.VIEW_COUNT = res.VIEW_COUNT;
                myCollectRes.VIEW_COUNT = res.VIEW_COUNT;
                yield this.articleRepository.save(res);
                yield this.postListRepository.save(postRes);
                yield this.myCollectRepository.save(myCollectRes);
                yield queryRunner.commitTransaction();
                return res;
            }
            catch (err) {
                yield queryRunner.rollbackTransaction();
            }
            finally {
                yield queryRunner.release();
            }
        });
    }
};
ArticleDetailService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(postList_entity_1.BbsPostList)),
    __param(1, typeorm_1.InjectRepository(articleDetail_entity_1.BbsArticleDetail)),
    __param(2, typeorm_1.InjectRepository(myCollectionList_entity_1.BbsMyCollectionList)),
    __param(3, typeorm_1.InjectRepository(myLikeList_entity_1.BbsMyLikeList)),
    __param(4, typeorm_1.InjectRepository(commentList_entity_1.BbsCommentsList)),
    __param(5, typeorm_1.InjectRepository(childrenComment_entity_1.BbsChildrenComments)),
    __param(6, typeorm_1.InjectRepository(user_entity_1.BbsUser)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository])
], ArticleDetailService);
exports.ArticleDetailService = ArticleDetailService;
//# sourceMappingURL=articleDetail.service.js.map