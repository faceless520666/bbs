"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const publish_service_1 = require("./publish.service");
const publish_controller_1 = require("./publish.controller");
const postList_entity_1 = require("../../entitys/postList.entity");
const articleDetail_entity_1 = require("../../entitys/articleDetail.entity");
const menuList_entity_1 = require("../../entitys/menuList.entity");
const user_entity_1 = require("../../entitys/user.entity");
const labelType_entity_1 = require("../../entitys/labelType.entity");
const labelList_entity_1 = require("../../entitys/labelList.entity");
let PublishModule = class PublishModule {
};
PublishModule = __decorate([
    common_1.Module({
        imports: [typeorm_1.TypeOrmModule.forFeature([postList_entity_1.BbsPostList, articleDetail_entity_1.BbsArticleDetail, menuList_entity_1.BbsMenu, user_entity_1.BbsUser, labelType_entity_1.BbsLabelType, labelList_entity_1.BbsLabelList])],
        providers: [publish_service_1.PublishService],
        controllers: [publish_controller_1.PublishController],
    })
], PublishModule);
exports.PublishModule = PublishModule;
//# sourceMappingURL=publish.module.js.map