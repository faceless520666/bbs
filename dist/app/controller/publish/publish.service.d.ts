import { Repository } from 'typeorm';
import { BbsPostList } from '../../entitys/postList.entity';
import { BbsArticleDetail } from '../../entitys/articleDetail.entity';
import { BbsMenu } from '../../entitys/menuList.entity';
import { BbsUser } from '../../entitys/user.entity';
import { BbsLabelType } from '../../entitys/labelType.entity';
import { BbsLabelList } from '../../entitys/labelList.entity';
export declare class PublishService {
    private readonly postsRepository;
    private readonly articleRepository;
    private readonly menuRepository;
    private readonly userRepository;
    private readonly labelTypeRepository;
    private readonly labelListRepository;
    constructor(postsRepository: Repository<BbsPostList>, articleRepository: Repository<BbsArticleDetail>, menuRepository: Repository<BbsMenu>, userRepository: Repository<BbsUser>, labelTypeRepository: Repository<BbsLabelType>, labelListRepository: Repository<BbsLabelList>);
    getMenu(): Promise<any>;
    getUser(param: any): Promise<any>;
    getLabel(): Promise<any>;
    editArticle(data: any): Promise<any>;
    publish(data: any): Promise<any>;
    addArticleDetail(data: any): Promise<any>;
}
