"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const api_error_code_enum_1 = require("../../../bing/common/enums/api-error-code.enum");
const typeorm_2 = require("typeorm");
const postList_entity_1 = require("../../entitys/postList.entity");
const articleDetail_entity_1 = require("../../entitys/articleDetail.entity");
const menuList_entity_1 = require("../../entitys/menuList.entity");
const user_entity_1 = require("../../entitys/user.entity");
const labelType_entity_1 = require("../../entitys/labelType.entity");
const labelList_entity_1 = require("../../entitys/labelList.entity");
let PublishService = class PublishService {
    constructor(postsRepository, articleRepository, menuRepository, userRepository, labelTypeRepository, labelListRepository) {
        this.postsRepository = postsRepository;
        this.articleRepository = articleRepository;
        this.menuRepository = menuRepository;
        this.userRepository = userRepository;
        this.labelTypeRepository = labelTypeRepository;
        this.labelListRepository = labelListRepository;
    }
    getMenu() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.menuRepository.find({ IS_TYPE: true });
        });
    }
    getUser(param) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.userRepository.findOne({ NICK_NAME: param.nickName });
        });
    }
    getLabel() {
        return __awaiter(this, void 0, void 0, function* () {
            const labelType = yield this.labelTypeRepository.find();
            for (const type of labelType) {
                const labelList = yield this.labelListRepository.find({ TYPE_ID: type.TYPE_ID });
                type.labelArr = labelList;
            }
            return labelType;
        });
    }
    editArticle(data) {
        return __awaiter(this, void 0, void 0, function* () {
            if (data) {
                const res = yield this.articleRepository.findOne({ ARTICLE_ID: data });
                return res;
            }
        });
    }
    publish(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const connection = typeorm_2.getConnection();
            const queryRunner = connection.createQueryRunner();
            yield queryRunner.connect();
            yield queryRunner.startTransaction();
            try {
                const userInfo = yield this.userRepository.findOne({ USER_ID: data.userId });
                data.author = userInfo.NICK_NAME;
                const msg = {
                    code: 1,
                    message: '',
                    articleId: '',
                };
                let isRes = yield this.postsRepository.findOne({ ARTICLE_ID: data.articleId });
                if (data.isEdit !== 'false') {
                    isRes = Object.assign(isRes, data);
                    isRes.IS_DRAFTS = data.isDrafts === 'false' ? false : true;
                    yield this.postsRepository.save(isRes);
                    yield this.addArticleDetail(isRes);
                    msg.code = api_error_code_enum_1.ApiErrorCode.SUCCESS;
                    if (data.isDrafts !== 'false') {
                        msg.articleId = data.articleId;
                        msg.message = '保存成功！';
                    }
                    else {
                        msg.message = '发表成功！';
                        msg.articleId = data.articleId;
                    }
                }
                else {
                    data.editTime = data.publishTime;
                    data.editPerson = data.userId;
                    let publishData = {
                        USER_ID: data.userId,
                        IS_EDIT: data.isEdit,
                        IS_DRAFTS: data.isDrafts,
                        ARTICLE_ID: data.articleId,
                        ARTICLE_TITLE: data.articleTitle,
                        ARTICLE_CONTENT: data.articleContent,
                        ARTICLE_TYPE: data.articleType,
                        ARTICLE_LABEL: data.articleLabel,
                        CREATED: data.publishTime,
                        AUTHOR: data.author,
                        EDIT_TIME: data.editTime,
                        EDIT_PERSON: data.editPerson,
                    };
                    if (isRes) {
                        publishData = Object.assign(isRes, publishData);
                    }
                    if (data.isDrafts !== 'false') {
                        publishData.IS_DRAFTS = true;
                        msg.code = api_error_code_enum_1.ApiErrorCode.SUCCESS;
                        msg.articleId = data.articleId;
                        msg.message = '保存成功！';
                    }
                    else {
                        publishData.IS_DRAFTS = false;
                        msg.code = api_error_code_enum_1.ApiErrorCode.SUCCESS;
                        msg.articleId = data.articleId;
                        msg.message = '发表成功！';
                    }
                    yield this.addArticleDetail(publishData);
                    const res = yield this.postsRepository.save(publishData);
                }
                yield queryRunner.commitTransaction();
                return msg;
            }
            catch (err) {
                yield queryRunner.rollbackTransaction();
            }
            finally {
                yield queryRunner.release();
            }
        });
    }
    addArticleDetail(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const connection = typeorm_2.getConnection();
            const queryRunner = connection.createQueryRunner();
            yield queryRunner.connect();
            yield queryRunner.startTransaction();
            try {
                if (data.IS_EDIT === 'false') {
                    const articleDetail = new articleDetail_entity_1.BbsArticleDetail();
                    const addObj = Object.assign(articleDetail, data);
                    yield this.postsRepository.manager.save(addObj);
                }
                else {
                    let editRes = yield this.articleRepository.findOne({ ARTICLE_ID: data.ARTICLE_ID });
                    editRes = Object.assign(editRes, data);
                    yield this.articleRepository.save(editRes);
                }
                yield queryRunner.commitTransaction();
            }
            catch (err) {
                yield queryRunner.rollbackTransaction();
            }
            finally {
                yield queryRunner.release();
            }
        });
    }
};
PublishService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(postList_entity_1.BbsPostList)),
    __param(1, typeorm_1.InjectRepository(articleDetail_entity_1.BbsArticleDetail)),
    __param(2, typeorm_1.InjectRepository(menuList_entity_1.BbsMenu)),
    __param(3, typeorm_1.InjectRepository(user_entity_1.BbsUser)),
    __param(4, typeorm_1.InjectRepository(labelType_entity_1.BbsLabelType)),
    __param(5, typeorm_1.InjectRepository(labelList_entity_1.BbsLabelList)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository])
], PublishService);
exports.PublishService = PublishService;
//# sourceMappingURL=publish.service.js.map