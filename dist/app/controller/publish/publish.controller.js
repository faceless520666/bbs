"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const publish_service_1 = require("./publish.service");
const bing_1 = require("../../../bing");
const uploadConfig_1 = require("../../../bing/common/uploadConfig");
let PublishController = class PublishController {
    constructor(postsRepository) {
        this.postsRepository = postsRepository;
    }
    index(req, res, param) {
        return __awaiter(this, void 0, void 0, function* () {
            const obj = {
                menuList: [],
                getLabel: null,
                renderData: null,
            };
            obj.getLabel = JSON.stringify(yield this.postsRepository.getLabel());
            obj.menuList = yield this.postsRepository.getMenu();
            if (param['0'] !== '') {
                obj.renderData = yield this.postsRepository.editArticle(param['0']);
            }
            console.log(111, obj);
            res.render('publish/publish', { title: 'publish', obj });
        });
    }
    publish(params) {
        return __awaiter(this, void 0, void 0, function* () {
            if (params.isEdit === 'false' || !params.isEdit) {
                params.publishTime = yield bing_1.util.dateType.getTime();
            }
            if (!params.articleId) {
                params.articleId = bing_1.util.ramdom.random(6) + bing_1.util.dateType.getTime();
            }
            params.articleLabel = params.articleLabel ? params.articleLabel.join(',') : '';
            return this.postsRepository.publish(params);
        });
    }
    uploadFile(file, data) {
        return __awaiter(this, void 0, void 0, function* () {
            return {
                code: 10000,
                message: '上传成功！',
                path: file.originalname,
                filename: file.originalname,
            };
        });
    }
    uploadVideo(file, data) {
        return __awaiter(this, void 0, void 0, function* () {
            return {
                code: 10000,
                message: '上传成功！',
                path: file.originalname,
                filename: file.originalname,
            };
        });
    }
    deleteUrl(params) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const msg = {
                    code: 10000,
                    message: '删除成功！',
                };
                return bing_1.util.client.remove(params.fid)
                    .then((body) => {
                    return msg;
                });
            }
            catch (e) {
                return e;
            }
        });
    }
};
__decorate([
    common_1.Get('/publish/*'),
    swagger_1.ApiOperation({ title: 'get balance from postList' }),
    __param(0, common_1.Request()), __param(1, common_1.Response()), __param(2, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], PublishController.prototype, "index", null);
__decorate([
    common_1.Post('publish'),
    swagger_1.ApiOperation({ title: 'get balance from postList' }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], PublishController.prototype, "publish", null);
__decorate([
    common_1.Post('articleImg'),
    common_1.UseInterceptors(common_1.FileInterceptor('file', uploadConfig_1.uploadConfig)),
    __param(0, common_1.UploadedFile()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], PublishController.prototype, "uploadFile", null);
__decorate([
    common_1.Post('articleVideo'),
    common_1.UseInterceptors(common_1.FileInterceptor('file', uploadConfig_1.uploadConfig)),
    __param(0, common_1.UploadedFile()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], PublishController.prototype, "uploadVideo", null);
__decorate([
    common_1.Post('deleteUrl'),
    swagger_1.ApiOperation({ title: 'get balance from data' }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], PublishController.prototype, "deleteUrl", null);
PublishController = __decorate([
    swagger_1.ApiUseTags('post'),
    common_1.Controller('post'),
    __metadata("design:paramtypes", [publish_service_1.PublishService])
], PublishController);
exports.PublishController = PublishController;
//# sourceMappingURL=publish.controller.js.map