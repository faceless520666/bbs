import { PublishService } from './publish.service';
import { BbsPostList } from '../../entitys/postList.entity';
export declare class PublishController {
    private readonly postsRepository;
    constructor(postsRepository: PublishService);
    index(req: any, res: any, param: any): Promise<any>;
    publish(params: any): Promise<BbsPostList[]>;
    uploadFile(file: any, data: any): Promise<{
        code: number;
        message: string;
        path: any;
        filename: any;
    }>;
    uploadVideo(file: any, data: any): Promise<{
        code: number;
        message: string;
        path: any;
        filename: any;
    }>;
    deleteUrl(params: any): Promise<any>;
}
