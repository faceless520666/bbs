import { Repository } from 'typeorm';
import { BbsUser } from '../../entitys/user.entity';
export declare class AuthService {
    private readonly authRepository;
    user: BbsUser;
    constructor(authRepository: Repository<BbsUser>);
    validate(payload: any): Promise<boolean>;
}
