import { Strategy } from 'passport-jwt';
import { AuthService } from './auth.service';
import { JwtPayload } from './jwt-payload.interface';
import { LoginService } from '../login/login.service';
declare const JwtStrategy_base: new (...args: any[]) => typeof Strategy;
export declare class JwtStrategy extends JwtStrategy_base {
    private readonly authService;
    private readonly usersServices;
    constructor(authService: AuthService, usersServices: LoginService);
    validate(payload: JwtPayload): Promise<true>;
}
export declare const callback: (err: any, user: any, info: any) => any;
export {};
