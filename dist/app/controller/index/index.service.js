"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const api_error_code_enum_1 = require("../../../bing/common/enums/api-error-code.enum");
const typeorm_2 = require("typeorm");
const postList_entity_1 = require("../../entitys/postList.entity");
const articleDetail_entity_1 = require("../../entitys/articleDetail.entity");
const menuList_entity_1 = require("../../entitys/menuList.entity");
const user_entity_1 = require("../../entitys/user.entity");
let IndexService = class IndexService {
    constructor(indexRepository, articleRepository, menuRepository, userRepository) {
        this.indexRepository = indexRepository;
        this.articleRepository = articleRepository;
        this.menuRepository = menuRepository;
        this.userRepository = userRepository;
    }
    getPostList(data) {
        return __awaiter(this, void 0, void 0, function* () {
            let res;
            let totalRes;
            const pageCount = data.pageCount ? data.pageCount * 1 : 10;
            const page = data.page ? data.page * 1 * pageCount : 0;
            const menuList = yield this.menuRepository.find();
            if (data.articleType && data.articleType !== 'ALL' && data.articleType !== 'HOT') {
                totalRes = yield this.indexRepository.find({ ARTICLE_TYPE: data.articleType, IS_DRAFTS: false });
                res = yield this.indexRepository
                    .createQueryBuilder('postList')
                    .where('postList.ARTICLE_TYPE = :ARTICLE_TYPE', { ARTICLE_TYPE: data.articleType })
                    .andWhere('postList.IS_DRAFTS = :IS_DRAFTS', { IS_DRAFTS: false })
                    .orderBy({
                    'postList.TOP': 'DESC',
                    'postList.ID': 'DESC',
                })
                    .skip(page)
                    .take(pageCount)
                    .getMany();
            }
            else {
                totalRes = yield this.indexRepository.find({ IS_DRAFTS: false });
                if (data.articleType !== 'HOT') {
                    res = yield this.indexRepository
                        .createQueryBuilder('postList')
                        .where('postList.IS_DRAFTS = :IS_DRAFTS', { IS_DRAFTS: false })
                        .orderBy({
                        'postList.TOP': 'DESC',
                        'postList.ID': 'DESC',
                    })
                        .skip(page)
                        .take(pageCount)
                        .getMany();
                }
                else {
                    res = yield this.indexRepository
                        .createQueryBuilder('postList')
                        .where('postList.IS_DRAFTS = :IS_DRAFTS', { IS_DRAFTS: false })
                        .orderBy({
                        'postList.TOP': 'DESC',
                        'postList.ID': 'DESC',
                    })
                        .skip(page)
                        .take(pageCount)
                        .getMany();
                }
            }
            for (const item of res) {
                const user = yield this.userRepository.findOne({ USER_ID: item.USER_ID });
                item.hearderIcon = user.HEADER_ICON ? user.HEADER_ICON : null;
                item.author = user.NICK_NAME;
            }
            const articleData = {
                code: api_error_code_enum_1.ApiErrorCode.SUCCESS,
                articleList: res,
                total: totalRes.length,
                menuList,
            };
            return articleData;
        });
    }
    viewCount(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const connection = typeorm_2.getConnection();
            const queryRunner = connection.createQueryRunner();
            yield queryRunner.connect();
            yield queryRunner.startTransaction();
            try {
                const res = yield this.indexRepository.findOne({ ARTICLE_ID: data.articleId });
                const upRes = yield this.articleRepository.findOne({ ARTICLE_ID: data.articleId });
                res.VIEW_COUNT += 1;
                upRes.VIEW_COUNT = res.VIEW_COUNT;
                yield this.indexRepository.save(res);
                yield this.articleRepository.save(upRes);
                yield queryRunner.commitTransaction();
            }
            catch (err) {
                yield queryRunner.rollbackTransaction();
            }
            finally {
                yield queryRunner.release();
            }
        });
    }
};
IndexService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(postList_entity_1.BbsPostList)),
    __param(1, typeorm_1.InjectRepository(articleDetail_entity_1.BbsArticleDetail)),
    __param(2, typeorm_1.InjectRepository(menuList_entity_1.BbsMenu)),
    __param(3, typeorm_1.InjectRepository(user_entity_1.BbsUser)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository])
], IndexService);
exports.IndexService = IndexService;
//# sourceMappingURL=index.service.js.map