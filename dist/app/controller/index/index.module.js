"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const index_service_1 = require("./index.service");
const index_controller_1 = require("./index.controller");
const postList_entity_1 = require("../../entitys/postList.entity");
const articleDetail_entity_1 = require("../../entitys/articleDetail.entity");
const menuList_entity_1 = require("../../entitys/menuList.entity");
const user_entity_1 = require("../../entitys/user.entity");
let IndexModule = class IndexModule {
};
IndexModule = __decorate([
    common_1.Module({
        imports: [typeorm_1.TypeOrmModule.forFeature([postList_entity_1.BbsPostList, articleDetail_entity_1.BbsArticleDetail, menuList_entity_1.BbsMenu, user_entity_1.BbsUser])],
        providers: [index_service_1.IndexService],
        controllers: [index_controller_1.IndexController],
    })
], IndexModule);
exports.IndexModule = IndexModule;
//# sourceMappingURL=index.module.js.map