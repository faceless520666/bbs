"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const index_service_1 = require("./index.service");
let IndexController = class IndexController {
    constructor(indexRepository) {
        this.indexRepository = indexRepository;
    }
    index(req, res, data) {
        return __awaiter(this, void 0, void 0, function* () {
            const postList = yield this.indexRepository.getPostList(data);
            postList.articleList.forEach((postItem, ind) => {
                postList.articleList[ind].ARTICLE_LABEL = postItem.ARTICLE_LABEL.split(',');
            });
            res.render('index/index', { title: 'index', postList });
        });
    }
    getPostList(req, res, param) {
        return __awaiter(this, void 0, void 0, function* () {
            const postList = yield this.indexRepository.getPostList(param);
            postList.articleList.forEach((postItem, ind) => {
                postList.articleList[ind].ARTICLE_LABEL = postItem.ARTICLE_LABEL.split(',');
            });
            res.render('index/index', { title: 'index', postList });
        });
    }
    getPageList(param) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.indexRepository.getPostList(param);
        });
    }
    viewCount(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const postList = yield this.indexRepository.viewCount(data);
        });
    }
};
__decorate([
    common_1.Get('/'),
    swagger_1.ApiOperation({ title: 'get balance from login' }),
    __param(0, common_1.Request()), __param(1, common_1.Response()), __param(2, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], IndexController.prototype, "index", null);
__decorate([
    common_1.Get('topic/:articleType'),
    swagger_1.ApiOperation({ title: 'get balance from postList' }),
    __param(0, common_1.Request()), __param(1, common_1.Response()), __param(2, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], IndexController.prototype, "getPostList", null);
__decorate([
    common_1.Get('topic/:articleType/:page'),
    swagger_1.ApiOperation({ title: 'get balance from postList' }),
    __param(0, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], IndexController.prototype, "getPageList", null);
__decorate([
    common_1.Post('/viewCount'),
    swagger_1.ApiOperation({ title: 'get balance from postList' }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], IndexController.prototype, "viewCount", null);
IndexController = __decorate([
    swagger_1.ApiUseTags('/'),
    common_1.Controller(),
    __metadata("design:paramtypes", [index_service_1.IndexService])
], IndexController);
exports.IndexController = IndexController;
//# sourceMappingURL=index.controller.js.map