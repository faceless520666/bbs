import { Repository } from 'typeorm';
import { BbsPostList } from '../../entitys/postList.entity';
import { BbsArticleDetail } from '../../entitys/articleDetail.entity';
import { BbsMenu } from '../../entitys/menuList.entity';
import { BbsUser } from '../../entitys/user.entity';
export declare class IndexService {
    private readonly indexRepository;
    private readonly articleRepository;
    private readonly menuRepository;
    private readonly userRepository;
    constructor(indexRepository: Repository<BbsPostList>, articleRepository: Repository<BbsArticleDetail>, menuRepository: Repository<BbsMenu>, userRepository: Repository<BbsUser>);
    getPostList(data: any): Promise<any>;
    viewCount(data: any): Promise<any>;
}
