import { IndexService } from './index.service';
export declare class IndexController {
    private readonly indexRepository;
    constructor(indexRepository: IndexService);
    index(req: any, res: any, data: any): Promise<any>;
    getPostList(req: any, res: any, param: any): Promise<any>;
    getPageList(param: any): Promise<any>;
    viewCount(data: any): Promise<any>;
}
