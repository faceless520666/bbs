"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const api_exception_1 = require("../../../bing/common/enums/api.exception");
const api_error_code_enum_1 = require("../../../bing/common/enums/api-error-code.enum");
const typeorm_2 = require("typeorm");
const myCollectionList_entity_1 = require("../../entitys/myCollectionList.entity");
const user_entity_1 = require("../../entitys/user.entity");
let CollectService = class CollectService {
    constructor(collectRepository, userRepository) {
        this.collectRepository = collectRepository;
        this.userRepository = userRepository;
    }
    account(data) {
        return __awaiter(this, void 0, void 0, function* () {
            return '';
        });
    }
    getCollectList(param) {
        return __awaiter(this, void 0, void 0, function* () {
            let res;
            let totalRes;
            const pageCount = param.pageCount ? param.pageCount * 1 : 10;
            const page = param.page ? (param.page - 1) * 1 * pageCount : 0;
            const user = yield this.userRepository.findOne({ NICK_NAME: param.nickName });
            totalRes = yield this.collectRepository.find({ USER_ID: user.USER_ID });
            res = yield this.collectRepository
                .createQueryBuilder('collectList')
                .where('collectList.USER_ID = :USER_ID', { USER_ID: user.USER_ID })
                .orderBy('collectList.ID', 'DESC')
                .skip(page)
                .take(pageCount)
                .getMany();
            const articleData = {
                articleList: res,
                total: totalRes.length,
            };
            return articleData;
        });
    }
    removeCollect(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const res = yield this.collectRepository.find({ ARTICLE_ID: data.articleId });
            const result = yield this.collectRepository.remove(res);
            if (result.length > 0) {
                return { code: 10000, message: '取消成功！' };
            }
            else {
                throw new api_exception_1.ApiException('取消失败', api_error_code_enum_1.ApiErrorCode.REMOVE_FAILT, common_1.HttpStatus.BAD_REQUEST);
            }
        });
    }
};
CollectService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(myCollectionList_entity_1.BbsMyCollectionList)),
    __param(1, typeorm_1.InjectRepository(user_entity_1.BbsUser)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository])
], CollectService);
exports.CollectService = CollectService;
//# sourceMappingURL=collect.service.js.map