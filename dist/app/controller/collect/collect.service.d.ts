import { Repository } from 'typeorm';
import { BbsMyCollectionList } from '../../entitys/myCollectionList.entity';
import { BbsUser } from '../../entitys/user.entity';
export declare class CollectService {
    private readonly collectRepository;
    private readonly userRepository;
    constructor(collectRepository: Repository<BbsMyCollectionList>, userRepository: Repository<BbsUser>);
    account(data: any): Promise<any>;
    getCollectList(param: any): Promise<any>;
    removeCollect(data: any): Promise<any>;
}
