import { CollectService } from './collect.service';
export declare class ReplyController {
    private readonly collectService;
    constructor(collectService: CollectService);
    index(req: any, res: any, data: any): Promise<any>;
    getArticle(param: any): Promise<any>;
    addView(param: any): Promise<ReplyController[]>;
}
