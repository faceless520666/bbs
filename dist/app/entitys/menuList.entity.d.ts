export declare class BbsMenu {
    ID: number;
    MENU_EN: string;
    MENU_CN: string;
    MENU_ID: string;
    IS_TYPE: boolean;
    LANGUE: string;
    STATE: number;
    CREATED: string;
}
