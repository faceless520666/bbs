"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const commentList_entity_1 = require("./commentList.entity");
let BbsChildrenComments = class BbsChildrenComments {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], BbsChildrenComments.prototype, "ID", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsChildrenComments.prototype, "USER_ID", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsChildrenComments.prototype, "COMMENT_USER_NAME", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsChildrenComments.prototype, "COMMENTATOR_ID", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsChildrenComments.prototype, "COMMENTATOR_NAME", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsChildrenComments.prototype, "COMMENT_ID", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsChildrenComments.prototype, "CHILD_COMMENT_ID", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsChildrenComments.prototype, "AUTHOR", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsChildrenComments.prototype, "AUTHOR_ID", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsChildrenComments.prototype, "ARTICLE_ID", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsChildrenComments.prototype, "COMMENT_CONTENT", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsChildrenComments.prototype, "CREATED", void 0);
__decorate([
    typeorm_1.Column({ default: 1 }),
    __metadata("design:type", Number)
], BbsChildrenComments.prototype, "STATE", void 0);
__decorate([
    typeorm_1.ManyToOne(type => commentList_entity_1.BbsCommentsList, parent => parent.childrenComentList),
    __metadata("design:type", commentList_entity_1.BbsCommentsList)
], BbsChildrenComments.prototype, "childrens", void 0);
BbsChildrenComments = __decorate([
    typeorm_1.Entity()
], BbsChildrenComments);
exports.BbsChildrenComments = BbsChildrenComments;
//# sourceMappingURL=childrenComment.entity.js.map