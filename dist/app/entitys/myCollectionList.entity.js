"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
let BbsMyCollectionList = class BbsMyCollectionList {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], BbsMyCollectionList.prototype, "ID", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsMyCollectionList.prototype, "USER_ID", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsMyCollectionList.prototype, "AUTHOR", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsMyCollectionList.prototype, "AUTHOR_ID", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsMyCollectionList.prototype, "ARTICLE_ID", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsMyCollectionList.prototype, "ARTICLE_TITLE", void 0);
__decorate([
    typeorm_1.Column({ length: 12000 }),
    __metadata("design:type", String)
], BbsMyCollectionList.prototype, "ARTICLE_CONTENT", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsMyCollectionList.prototype, "ARTICLE_LABEL", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsMyCollectionList.prototype, "ARTICLE_TYPE", void 0);
__decorate([
    typeorm_1.Column({ default: 0 }),
    __metadata("design:type", Number)
], BbsMyCollectionList.prototype, "VIEW_COUNT", void 0);
__decorate([
    typeorm_1.Column({ default: 0 }),
    __metadata("design:type", Number)
], BbsMyCollectionList.prototype, "COLLECT_COUNT", void 0);
__decorate([
    typeorm_1.Column({ default: 0 }),
    __metadata("design:type", Number)
], BbsMyCollectionList.prototype, "LIKE_COUNT", void 0);
__decorate([
    typeorm_1.Column({ default: 0 }),
    __metadata("design:type", Number)
], BbsMyCollectionList.prototype, "COMMENT_COUNT", void 0);
__decorate([
    typeorm_1.Column({ default: false }),
    __metadata("design:type", Boolean)
], BbsMyCollectionList.prototype, "IS_COLLECT", void 0);
__decorate([
    typeorm_1.Column({ default: 1 }),
    __metadata("design:type", Number)
], BbsMyCollectionList.prototype, "STATE", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsMyCollectionList.prototype, "CREATED", void 0);
BbsMyCollectionList = __decorate([
    typeorm_1.Entity()
], BbsMyCollectionList);
exports.BbsMyCollectionList = BbsMyCollectionList;
//# sourceMappingURL=myCollectionList.entity.js.map