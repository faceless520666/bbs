import { BbsArticleDetail } from './articleDetail.entity';
import { BbsChildrenComments } from './childrenComment.entity';
export declare class BbsCommentsList {
    ID: number;
    USER_ID: string;
    COMMENT_USER_NAME: string;
    COMMENTATOR_ID: string;
    COMMENTATOR_NAME: string;
    ARTICLE_TITLE: string;
    ARTICLE_ID: string;
    COMMENT_ID: string;
    COMMENT_CONTENT: string;
    CREATED: string;
    STATE: number;
    posts: BbsArticleDetail;
    childrenComentList: BbsChildrenComments[];
}
