"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const labelList_entity_1 = require("./labelList.entity");
let BbsLabelType = class BbsLabelType {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], BbsLabelType.prototype, "ID", void 0);
__decorate([
    typeorm_1.Column({ length: 20 }),
    __metadata("design:type", String)
], BbsLabelType.prototype, "TYPE_NAME_EN", void 0);
__decorate([
    typeorm_1.Column({ length: 20 }),
    __metadata("design:type", String)
], BbsLabelType.prototype, "TYPE_NAME_CN", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsLabelType.prototype, "TYPE_ID", void 0);
__decorate([
    typeorm_1.Column({ default: 'zh_CN' }),
    __metadata("design:type", String)
], BbsLabelType.prototype, "LANGUE", void 0);
__decorate([
    typeorm_1.Column({ default: 1 }),
    __metadata("design:type", Number)
], BbsLabelType.prototype, "STATE", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsLabelType.prototype, "CREATED", void 0);
__decorate([
    typeorm_1.OneToMany(type => labelList_entity_1.BbsLabelList, children => children.labelList),
    __metadata("design:type", Array)
], BbsLabelType.prototype, "labelArr", void 0);
BbsLabelType = __decorate([
    typeorm_1.Entity()
], BbsLabelType);
exports.BbsLabelType = BbsLabelType;
//# sourceMappingURL=labelType.entity.js.map