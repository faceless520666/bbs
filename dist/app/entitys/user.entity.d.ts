export declare class BbsUser {
    ID: number;
    TOKEN: string;
    USER_ID: string;
    PASSWORD: string;
    NICK_NAME: string;
    EMAIL: string;
    HEADER_ICON: string;
    PERSONAL_PROFILE: string;
    TEL: string;
    FOCUS_COUNT: number;
    ARTICLE_COUNT: number;
    HAD_NEWS: boolean;
    CREATED: string;
    STATE: number;
    ACTIVITY: boolean;
}
