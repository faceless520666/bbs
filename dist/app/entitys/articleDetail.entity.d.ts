import { BbsPostList } from './postList.entity';
import { BbsCommentsList } from './commentList.entity';
export declare class BbsArticleDetail {
    ID: number;
    USER_ID: string;
    ARTICLE_TITLE: string;
    ARTICLE_ID: string;
    ARTICLE_CONTENT: string;
    ARTICLE_TYPE: string;
    ARTICLE_LABEL: string;
    AUTHOR: string;
    COMMENT_ID: string;
    VIEW_COUNT: number;
    COLLECT_COUNT: number;
    LIKE_COUNT: number;
    COMMENT_COUNT: number;
    IS_COLLECT: boolean;
    ID_DRAFTS: boolean;
    CREATED: string;
    STATE: number;
    post: BbsPostList[];
    comments: BbsCommentsList[];
}
