"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
let BbsUser = class BbsUser {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], BbsUser.prototype, "ID", void 0);
__decorate([
    typeorm_1.Column({ default: null }),
    __metadata("design:type", String)
], BbsUser.prototype, "TOKEN", void 0);
__decorate([
    typeorm_1.Column({ length: 20 }),
    __metadata("design:type", String)
], BbsUser.prototype, "USER_ID", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsUser.prototype, "PASSWORD", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsUser.prototype, "NICK_NAME", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsUser.prototype, "EMAIL", void 0);
__decorate([
    typeorm_1.Column({ default: null }),
    __metadata("design:type", String)
], BbsUser.prototype, "HEADER_ICON", void 0);
__decorate([
    typeorm_1.Column({ default: '这个人很懒！' }),
    __metadata("design:type", String)
], BbsUser.prototype, "PERSONAL_PROFILE", void 0);
__decorate([
    typeorm_1.Column({ default: '0' }),
    __metadata("design:type", String)
], BbsUser.prototype, "TEL", void 0);
__decorate([
    typeorm_1.Column({ default: 0 }),
    __metadata("design:type", Number)
], BbsUser.prototype, "FOCUS_COUNT", void 0);
__decorate([
    typeorm_1.Column({ default: 0 }),
    __metadata("design:type", Number)
], BbsUser.prototype, "ARTICLE_COUNT", void 0);
__decorate([
    typeorm_1.Column({ default: false }),
    __metadata("design:type", Boolean)
], BbsUser.prototype, "HAD_NEWS", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsUser.prototype, "CREATED", void 0);
__decorate([
    typeorm_1.Column({ default: 1 }),
    __metadata("design:type", Number)
], BbsUser.prototype, "STATE", void 0);
__decorate([
    typeorm_1.Column({ default: true }),
    __metadata("design:type", Boolean)
], BbsUser.prototype, "ACTIVITY", void 0);
BbsUser = __decorate([
    typeorm_1.Entity()
], BbsUser);
exports.BbsUser = BbsUser;
//# sourceMappingURL=user.entity.js.map