import { BbsLabelType } from './labelType.entity';
export declare class BbsLabelList {
    ID: number;
    LABEL_NAME: string;
    TYPE_ID: string;
    LABEL_ID: string;
    STATE: number;
    CREATED: string;
    labelList: BbsLabelType;
}
