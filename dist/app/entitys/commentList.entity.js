"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const articleDetail_entity_1 = require("./articleDetail.entity");
const childrenComment_entity_1 = require("./childrenComment.entity");
let BbsCommentsList = class BbsCommentsList {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], BbsCommentsList.prototype, "ID", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsCommentsList.prototype, "USER_ID", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsCommentsList.prototype, "COMMENT_USER_NAME", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsCommentsList.prototype, "COMMENTATOR_ID", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsCommentsList.prototype, "COMMENTATOR_NAME", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsCommentsList.prototype, "ARTICLE_TITLE", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsCommentsList.prototype, "ARTICLE_ID", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsCommentsList.prototype, "COMMENT_ID", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsCommentsList.prototype, "COMMENT_CONTENT", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsCommentsList.prototype, "CREATED", void 0);
__decorate([
    typeorm_1.Column({ default: 1 }),
    __metadata("design:type", Number)
], BbsCommentsList.prototype, "STATE", void 0);
__decorate([
    typeorm_1.ManyToOne(type => articleDetail_entity_1.BbsArticleDetail, post => post.comments),
    __metadata("design:type", articleDetail_entity_1.BbsArticleDetail)
], BbsCommentsList.prototype, "posts", void 0);
__decorate([
    typeorm_1.OneToMany(type => childrenComment_entity_1.BbsChildrenComments, children => children.childrens),
    __metadata("design:type", Array)
], BbsCommentsList.prototype, "childrenComentList", void 0);
BbsCommentsList = __decorate([
    typeorm_1.Entity()
], BbsCommentsList);
exports.BbsCommentsList = BbsCommentsList;
//# sourceMappingURL=commentList.entity.js.map