"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const articleDetail_entity_1 = require("./articleDetail.entity");
let BbsPostList = class BbsPostList {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], BbsPostList.prototype, "ID", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsPostList.prototype, "USER_ID", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsPostList.prototype, "ARTICLE_TITLE", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsPostList.prototype, "ARTICLE_ID", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsPostList.prototype, "ARTICLE_TYPE", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsPostList.prototype, "ARTICLE_LABEL", void 0);
__decorate([
    typeorm_1.Column({ length: 12000 }),
    __metadata("design:type", String)
], BbsPostList.prototype, "ARTICLE_CONTENT", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsPostList.prototype, "AUTHOR", void 0);
__decorate([
    typeorm_1.Column({ default: 0 }),
    __metadata("design:type", Number)
], BbsPostList.prototype, "VIEW_COUNT", void 0);
__decorate([
    typeorm_1.Column({ default: 0 }),
    __metadata("design:type", Number)
], BbsPostList.prototype, "COLLECT_COUNT", void 0);
__decorate([
    typeorm_1.Column({ default: 0 }),
    __metadata("design:type", Number)
], BbsPostList.prototype, "LIKE_COUNT", void 0);
__decorate([
    typeorm_1.Column({ default: 0 }),
    __metadata("design:type", Number)
], BbsPostList.prototype, "COMMENT_COUNT", void 0);
__decorate([
    typeorm_1.Column({ default: false }),
    __metadata("design:type", Boolean)
], BbsPostList.prototype, "IS_DRAFTS", void 0);
__decorate([
    typeorm_1.Column({ default: false }),
    __metadata("design:type", Boolean)
], BbsPostList.prototype, "TOP", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsPostList.prototype, "CREATED", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsPostList.prototype, "EDIT_TIME", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsPostList.prototype, "EDIT_PERSON", void 0);
__decorate([
    typeorm_1.Column({ default: 1 }),
    __metadata("design:type", Number)
], BbsPostList.prototype, "STATE", void 0);
__decorate([
    typeorm_1.OneToOne(type => articleDetail_entity_1.BbsArticleDetail, acticle => acticle.post, {
        cascade: true,
    }),
    __metadata("design:type", articleDetail_entity_1.BbsArticleDetail)
], BbsPostList.prototype, "acticles", void 0);
BbsPostList = __decorate([
    typeorm_1.Entity()
], BbsPostList);
exports.BbsPostList = BbsPostList;
//# sourceMappingURL=postList.entity.js.map