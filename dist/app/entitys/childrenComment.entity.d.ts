import { BbsCommentsList } from './commentList.entity';
export declare class BbsChildrenComments {
    ID: number;
    USER_ID: string;
    COMMENT_USER_NAME: string;
    COMMENTATOR_ID: string;
    COMMENTATOR_NAME: string;
    COMMENT_ID: string;
    CHILD_COMMENT_ID: string;
    AUTHOR: string;
    AUTHOR_ID: string;
    ARTICLE_ID: string;
    COMMENT_CONTENT: string;
    CREATED: string;
    STATE: number;
    childrens: BbsCommentsList;
}
