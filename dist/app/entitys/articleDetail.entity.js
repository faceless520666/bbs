"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const postList_entity_1 = require("./postList.entity");
const commentList_entity_1 = require("./commentList.entity");
let BbsArticleDetail = class BbsArticleDetail {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], BbsArticleDetail.prototype, "ID", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsArticleDetail.prototype, "USER_ID", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsArticleDetail.prototype, "ARTICLE_TITLE", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsArticleDetail.prototype, "ARTICLE_ID", void 0);
__decorate([
    typeorm_1.Column({ length: 12000 }),
    __metadata("design:type", String)
], BbsArticleDetail.prototype, "ARTICLE_CONTENT", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsArticleDetail.prototype, "ARTICLE_TYPE", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsArticleDetail.prototype, "ARTICLE_LABEL", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsArticleDetail.prototype, "AUTHOR", void 0);
__decorate([
    typeorm_1.Column({ default: null }),
    __metadata("design:type", String)
], BbsArticleDetail.prototype, "COMMENT_ID", void 0);
__decorate([
    typeorm_1.Column({ default: 0 }),
    __metadata("design:type", Number)
], BbsArticleDetail.prototype, "VIEW_COUNT", void 0);
__decorate([
    typeorm_1.Column({ default: 0 }),
    __metadata("design:type", Number)
], BbsArticleDetail.prototype, "COLLECT_COUNT", void 0);
__decorate([
    typeorm_1.Column({ default: 0 }),
    __metadata("design:type", Number)
], BbsArticleDetail.prototype, "LIKE_COUNT", void 0);
__decorate([
    typeorm_1.Column({ default: 0 }),
    __metadata("design:type", Number)
], BbsArticleDetail.prototype, "COMMENT_COUNT", void 0);
__decorate([
    typeorm_1.Column({ default: false }),
    __metadata("design:type", Boolean)
], BbsArticleDetail.prototype, "IS_COLLECT", void 0);
__decorate([
    typeorm_1.Column({ default: false }),
    __metadata("design:type", Boolean)
], BbsArticleDetail.prototype, "ID_DRAFTS", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], BbsArticleDetail.prototype, "CREATED", void 0);
__decorate([
    typeorm_1.Column({ default: 1 }),
    __metadata("design:type", Number)
], BbsArticleDetail.prototype, "STATE", void 0);
__decorate([
    typeorm_1.OneToOne(type => postList_entity_1.BbsPostList, post => post.acticles),
    __metadata("design:type", Array)
], BbsArticleDetail.prototype, "post", void 0);
__decorate([
    typeorm_1.OneToMany(type => commentList_entity_1.BbsCommentsList, comment => comment.posts),
    __metadata("design:type", Array)
], BbsArticleDetail.prototype, "comments", void 0);
BbsArticleDetail = __decorate([
    typeorm_1.Entity()
], BbsArticleDetail);
exports.BbsArticleDetail = BbsArticleDetail;
//# sourceMappingURL=articleDetail.entity.js.map