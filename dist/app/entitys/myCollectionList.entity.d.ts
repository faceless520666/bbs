export declare class BbsMyCollectionList {
    ID: number;
    USER_ID: string;
    AUTHOR: string;
    AUTHOR_ID: string;
    ARTICLE_ID: string;
    ARTICLE_TITLE: string;
    ARTICLE_CONTENT: string;
    ARTICLE_LABEL: string;
    ARTICLE_TYPE: string;
    VIEW_COUNT: number;
    COLLECT_COUNT: number;
    LIKE_COUNT: number;
    COMMENT_COUNT: number;
    IS_COLLECT: boolean;
    STATE: number;
    CREATED: string;
}
