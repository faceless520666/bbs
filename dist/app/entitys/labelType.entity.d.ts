import { BbsLabelList } from './labelList.entity';
export declare class BbsLabelType {
    ID: number;
    TYPE_NAME_EN: string;
    TYPE_NAME_CN: string;
    TYPE_ID: string;
    LANGUE: string;
    STATE: number;
    CREATED: string;
    labelArr: BbsLabelList[];
}
