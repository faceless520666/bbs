import { BbsArticleDetail } from './articleDetail.entity';
export declare class BbsPostList {
    ID: number;
    USER_ID: string;
    ARTICLE_TITLE: string;
    ARTICLE_ID: string;
    ARTICLE_TYPE: string;
    ARTICLE_LABEL: string;
    ARTICLE_CONTENT: string;
    AUTHOR: string;
    VIEW_COUNT: number;
    COLLECT_COUNT: number;
    LIKE_COUNT: number;
    COMMENT_COUNT: number;
    IS_DRAFTS: boolean;
    TOP: boolean;
    CREATED: string;
    EDIT_TIME: string;
    EDIT_PERSON: string;
    STATE: number;
    acticles: BbsArticleDetail;
}
