export declare class BbsMyLikeList {
    ID: number;
    USER_ID: string;
    ARTICLE_ID: string;
    LIKE_COUNT: number;
    IS_LIKE: boolean;
    STATE: number;
    CREATED: string;
}
