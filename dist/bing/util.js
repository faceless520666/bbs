"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const helper = require("./common/helper");
const encrypt = require("./common/encrypt");
const log_1 = require("./common/log");
const moment = require("moment");
const random = require("./common/random");
const dateType_1 = require("./common/dateType");
const SeaweedFS = require("seaweedfs");
class Util {
}
Util.helper = helper;
Util.log = new log_1.Log();
Util.encrypt = encrypt;
Util.moment = moment;
Util.ramdom = random;
Util.dateType = dateType_1.DataType;
Util.session = {
    secrets: '123',
};
Util.client = new SeaweedFS({
    masters: [{
            host: '7.7.3.12',
            port: 9333,
        },
    ],
    scheme: 'http',
    retry_count: 60,
    retry_timeout: 20000,
    log_name: 'SeaweedFS',
    log_level: 'info',
});
exports.Util = Util;
//# sourceMappingURL=util.js.map