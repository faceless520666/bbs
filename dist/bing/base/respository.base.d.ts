export declare abstract class RepositoryBase<T> {
    getAll(): Promise<any>;
}
