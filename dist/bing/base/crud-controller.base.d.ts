import { IKey, CrudServiceBase } from '@/bing';
import { ApiControllerBase } from './api-controller.base';
export declare class CrudControllerBase<T extends IKey> extends ApiControllerBase {
    private readonly crudService;
    constructor(crudService: CrudServiceBase<T>);
}
