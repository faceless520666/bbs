"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const api_controller_base_1 = require("./api-controller.base");
class CrudControllerBase extends api_controller_base_1.ApiControllerBase {
    constructor(crudService) {
        super();
        this.crudService = crudService;
    }
}
exports.CrudControllerBase = CrudControllerBase;
//# sourceMappingURL=crud-controller.base.js.map