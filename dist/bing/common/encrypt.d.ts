/// <reference types="node" />
import * as crypto from 'crypto';
export declare let md5: (value: string, encoding?: crypto.Utf8AsciiLatin1Encoding) => string;
export declare let sha1: (value: string, encoding?: crypto.Utf8AsciiLatin1Encoding) => string;
export declare let sha256: (value: string, encoding?: crypto.Utf8AsciiLatin1Encoding) => string;
export declare let sha512: (value: string, encoding?: crypto.Utf8AsciiLatin1Encoding) => string;
export declare let hmacMd5: (value: string, key: string, encoding?: crypto.Utf8AsciiLatin1Encoding) => string;
export declare let hmacSha1: (value: string, key: string, encoding?: crypto.Utf8AsciiLatin1Encoding) => string;
export declare let hmacSha256: (value: string, key: string, encoding?: crypto.Utf8AsciiLatin1Encoding) => string;
export declare let hmacSha512: (value: string, key: string, encoding?: crypto.Utf8AsciiLatin1Encoding) => string;
export declare let aesEncrypt: (value: string, key: string, encoding?: crypto.Utf8AsciiBinaryEncoding) => string;
export declare let aesDecrypt: (value: string, key: string, encoding?: crypto.Utf8AsciiBinaryEncoding) => string;
export declare let base64Encrypt: (value: string) => string;
export declare let base64Decrypt: (value: string) => string;
