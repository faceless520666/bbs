export declare let random: (length: number, options?: string | true | stringRandom.Options) => string;
declare namespace stringRandom {
    interface Options {
        specials?: string | boolean;
        number?: string | boolean;
        letters?: string | boolean;
    }
}
export {};
