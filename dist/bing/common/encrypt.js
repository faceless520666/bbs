"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crypto = require("crypto");
const index_1 = require("../index");
exports.md5 = (value, encoding = 'utf8') => {
    if (index_1.util.helper.isEmpty(value)) {
        return '';
    }
    const hash = crypto.createHash('md5');
    hash.update(value, encoding);
    return hash.digest('hex');
};
exports.sha1 = (value, encoding = 'utf8') => {
    if (index_1.util.helper.isEmpty(value)) {
        return '';
    }
    const hash = crypto.createHash('sha1');
    hash.update(value, encoding);
    return hash.digest('hex');
};
exports.sha256 = (value, encoding = 'utf8') => {
    if (index_1.util.helper.isEmpty(value)) {
        return '';
    }
    const hash = crypto.createHash('sha256');
    hash.update(value, encoding);
    return hash.digest('hex');
};
exports.sha512 = (value, encoding = 'utf8') => {
    if (index_1.util.helper.isEmpty(value)) {
        return '';
    }
    const hash = crypto.createHash('sha512');
    hash.update(value, encoding);
    return hash.digest('hex');
};
exports.hmacMd5 = (value, key, encoding = 'utf8') => {
    if (index_1.util.helper.isEmpty(value)) {
        return '';
    }
    const hmac = crypto.createHmac('md5', key);
    hmac.update(value);
    return hmac.digest('hex');
};
exports.hmacSha1 = (value, key, encoding = 'utf8') => {
    if (index_1.util.helper.isEmpty(value)) {
        return '';
    }
    const hmac = crypto.createHmac('sha1', key);
    hmac.update(value);
    return hmac.digest('hex');
};
exports.hmacSha256 = (value, key, encoding = 'utf8') => {
    if (index_1.util.helper.isEmpty(value)) {
        return '';
    }
    const hmac = crypto.createHmac('sha256', key);
    hmac.update(value);
    return hmac.digest('hex');
};
exports.hmacSha512 = (value, key, encoding = 'utf8') => {
    if (index_1.util.helper.isEmpty(value)) {
        return '';
    }
    const hmac = crypto.createHmac('sha512', key);
    hmac.update(value);
    return hmac.digest('hex');
};
exports.aesEncrypt = (value, key, encoding = 'utf8') => {
    if (index_1.util.helper.isEmpty(value)) {
        return '';
    }
    const cipher = crypto.createCipher('aes-192', key);
    let crypted = cipher.update(value, encoding, 'hex');
    crypted += cipher.final('hex');
    return crypted;
};
exports.aesDecrypt = (value, key, encoding = 'utf8') => {
    if (index_1.util.helper.isEmpty(value)) {
        return '';
    }
    const decipher = crypto.createDecipher('aes-192', key);
    let decrypt = decipher.update(value, 'hex', encoding);
    decrypt += decipher.final('utf8');
    return decrypt;
};
exports.base64Encrypt = (value) => {
    if (index_1.util.helper.isEmpty(value)) {
        return '';
    }
    return Buffer.from(value).toString('base64');
};
exports.base64Decrypt = (value) => {
    if (index_1.util.helper.isEmpty(value)) {
        return '';
    }
    return Buffer.from(value, 'base64').toString('utf8');
};
//# sourceMappingURL=encrypt.js.map