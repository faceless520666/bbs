import * as multer from 'multer';
export declare const uploadConfig: {
    fileFilter: (req: any, file: any, cb: any) => void;
    limits: {
        fileSize: number;
    };
    storage: multer.StorageEngine;
};
