"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class HttpClient {
}
exports.HttpClient = HttpClient;
class HttpRequest {
    constructor(httpMethod, url, body) {
        this.httpMethod = httpMethod;
        this.url = url;
        this.body = body;
        this.headers = {};
    }
}
exports.HttpRequest = HttpRequest;
class HttpHeaders {
}
exports.HttpHeaders = HttpHeaders;
class HttpHandleOptions {
}
exports.HttpHandleOptions = HttpHandleOptions;
var HttpMethod;
(function (HttpMethod) {
    HttpMethod[HttpMethod["Get"] = 0] = "Get";
    HttpMethod[HttpMethod["Post"] = 1] = "Post";
    HttpMethod[HttpMethod["Put"] = 2] = "Put";
    HttpMethod[HttpMethod["Delete"] = 3] = "Delete";
})(HttpMethod = exports.HttpMethod || (exports.HttpMethod = {}));
var HttpContentType;
(function (HttpContentType) {
    HttpContentType[HttpContentType["FormUrlEncoded"] = 0] = "FormUrlEncoded";
    HttpContentType[HttpContentType["Json"] = 1] = "Json";
})(HttpContentType = exports.HttpContentType || (exports.HttpContentType = {}));
//# sourceMappingURL=webapi.js.map