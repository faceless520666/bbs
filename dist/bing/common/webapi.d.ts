export declare class HttpClient {
}
export declare class HttpRequest<T> {
    private httpMethod;
    private url;
    private body?;
    private headers;
    private httpContentType;
    private parameters;
    constructor(httpMethod: HttpMethod, url: string, body?: any);
}
export declare class HttpHeaders {
}
export declare class HttpHandleOptions<T> {
    beforeHandler?: () => boolean;
    handler: (value: T) => void;
    failHandler?: (result: any) => void;
    completeHandler?: () => void;
}
export declare enum HttpMethod {
    Get = 0,
    Post = 1,
    Put = 2,
    Delete = 3
}
export declare enum HttpContentType {
    FormUrlEncoded = 0,
    Json = 1
}
