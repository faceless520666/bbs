"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _ = require("lodash");
const moment = require("moment");
const uuid_1 = require("./internal/uuid");
exports.isUndefined = (value) => {
    return typeof value === 'undefined';
};
exports.isString = (value) => {
    return typeof value === 'string';
};
exports.isEmpty = (value) => {
    if (typeof value === 'number') {
        return false;
    }
    if (value && value.trim) {
        value = value.trim();
    }
    if (!value) {
        return true;
    }
    if (value === '00000000-0000-0000-0000-000000000000') {
        return true;
    }
    return _.isEmpty(value);
};
exports.isNumber = (value) => {
    return !isNaN(parseFloat(value)) && isFinite(value);
};
exports.toNumber = (value, precision, isTruncate) => {
    if (!exports.isNumber(value)) {
        return 0;
    }
    value = value.toString();
    if (exports.isEmpty(precision)) {
        return parseFloat(value);
    }
    if (isTruncate) {
        return parseFloat(value.substring(0, value.indexOf('.') + parseInt(precision) + 1));
    }
    return parseFloat(parseFloat(value).toFixed(precision));
};
exports.toInt = (value) => {
    if (!exports.isNumber(value)) {
        return 0;
    }
    return _.round(value);
};
exports.floor = (value) => {
    if (!exports.isNumber(value)) {
        return 0;
    }
    return _.floor(value);
};
exports.toString = (value) => {
    return _.toString(value).trim();
};
exports.toBool = (value) => {
    if (value === true) {
        return true;
    }
    const strValue = exports.toString(value).toLowerCase();
    if (strValue === '1') {
        return true;
    }
    if (strValue === 'true') {
        return true;
    }
    if (strValue === '是') {
        return true;
    }
    if (strValue === 'yes') {
        return true;
    }
    if (strValue === 'ok') {
        return true;
    }
    return false;
};
exports.isArray = (value) => {
    return Array.isArray(value);
};
exports.isEmptyArray = (value) => {
    return exports.isArray(value) && value.length === 0;
};
exports.first = (array) => {
    return _.first(array);
};
exports.last = (array) => {
    return _.last(array);
};
exports.toJson = (value) => {
    return JSON.stringify(value);
};
exports.toObjectFromJson = (json) => {
    return JSON.parse(json);
};
exports.clone = (obj) => {
    return JSON.parse(JSON.stringify(obj));
};
exports.uuid = () => {
    return uuid_1.UUID.UUID();
};
exports.isValidDate = (date) => {
    return moment(exports.getValidDate(date)).isValid();
};
exports.getValidDate = (date) => {
    if (!date) {
        return date;
    }
    if (typeof date !== 'string') {
        return date;
    }
    if (date.indexOf('-') <= 0) {
        return date;
    }
    const regex = /(\d{4})-(\d{1,2})-(\d{1,2})(?:(?:\s+)(\d{1,2}):(\d{1,2}):?(\d{1,2})?)?/;
    if (!regex.test(date)) {
        return date;
    }
    const dateSegment = date.match(regex);
    if (!dateSegment) {
        return date;
    }
    const year = dateSegment[1];
    let month = dateSegment[2];
    let day = dateSegment[3];
    let hour = dateSegment[4];
    let minute = dateSegment[5];
    let second = dateSegment[6];
    month = month.length === 1 ? `0${month}` : month;
    day = day.length === 1 ? `0${day}` : day;
    let result = `${year}-${month}-${day}`;
    if (hour && minute) {
        hour = hour.length === 1 ? `0${hour}` : hour;
        minute = minute.length === 1 ? `0${minute}` : minute;
        result += ` ${hour}:${minute}`;
    }
    if (second) {
        second = second.length === 1 ? `0${second}` : second;
        result += `:${second}`;
    }
    return result;
};
exports.toDate = (date) => {
    return moment(exports.getValidDate(date)).toDate();
};
exports.formatDate = (datetime, format) => {
    const date = moment(exports.getValidDate(datetime));
    if (!date.isValid()) {
        return '';
    }
    return date.format(format);
};
exports.to = (value) => {
    return value;
};
exports.remove = (source, predicate) => {
    return _.remove(source, t => predicate(t));
};
exports.addToArray = (source, items) => {
    if (exports.isEmpty(items)) {
        return source;
    }
    if (!items.length) {
        source.push(items);
        return source;
    }
    items.forEarch(item => {
        if (exports.isEmpty(item)) {
            return;
        }
        source.push(item);
    });
    return source;
};
exports.clear = (array) => {
    if (array && array.length) {
        array.length = 0;
    }
};
exports.toList = (input) => {
    const result = new Array();
    if (!input) {
        return result;
    }
    const array = input.split(',');
    array.forEach(value => {
        if (!value) {
            return;
        }
        result.push(exports.to(value));
    });
    return result;
};
exports.except = (source, target, property) => {
    return _.differenceBy(getArray(source), getArray(target), property);
};
exports.exceptWith = (source, target, comparator) => {
    return _.differenceWith(getArray(source), getArray(target), comparator);
};
function getArray(array) {
    const list = new Array();
    if (array.length === undefined) {
        list.push(array);
        return list;
    }
    return array;
}
exports.concat = (source, target) => {
    return _.concat(source, target);
};
exports.groupBy = (source, property) => {
    const groups = _.groupBy(source, property);
    const result = new Map();
    for (const key in groups) {
        if (!key) {
            continue;
        }
        result.set(key, groups[key].map(t => t));
    }
    return result;
};
exports.distinct = (source, property) => {
    return _.uniqBy(source, property);
};
exports.random = (lower = 0, upper = 1, floating = false) => {
    return _.random(lower, upper, floating);
};
exports.getRangeDate = (range, type) => {
    const formatDate = (time) => {
        const currentDate = new Date(time);
        const year = currentDate.getFullYear();
        const month = (currentDate.getMonth() + 1) < 10 ? '0' + (currentDate.getMonth() + 1) : (currentDate.getMonth() + 1);
        const day = currentDate.getDate() < 10 ? '0' + currentDate.getDate() : currentDate.getDate();
        return `${year}-${month}-${day}`;
    };
    const resultArray = [];
    let changeDate;
    if (range) {
        if (type) {
            if (type === 'one') {
                changeDate = formatDate(new Date().getTime() + (1000 * 3600 * 24 * range));
                return changeDate;
            }
            if (type === 'more') {
                if (range < 0) {
                    for (let i = Math.abs(range); i >= 0; i--) {
                        resultArray.push(formatDate(new Date().getTime() + (-1000 * 3600 * 24 * i)));
                    }
                    return resultArray;
                }
                else {
                    for (let i = 1; i <= range; i++) {
                        resultArray.push(formatDate(new Date().getTime() + (1000 * 3600 * 24 * i)));
                    }
                    return resultArray;
                }
            }
        }
    }
    else {
        changeDate = formatDate(new Date().getTime() + (1000 * 3600 * 24 * range));
        return changeDate;
    }
};
exports.getRangeDateUtc = (range, type) => {
    const formatDate = (time) => {
        const currentDate = moment(time).utc().toDate();
        const year = currentDate.getFullYear();
        const month = (currentDate.getMonth() + 1) < 10 ? '0' + (currentDate.getMonth() + 1) : (currentDate.getMonth() + 1);
        const day = currentDate.getDate() < 10 ? '0' + currentDate.getDate() : currentDate.getDate();
        return `${year}-${month}-${day}`;
    };
    const resultArray = [];
    let changeDate;
    if (range) {
        if (type) {
            if (type === 'one') {
                changeDate = formatDate(moment().utc().valueOf() + (1000 * 3600 * 24 * range));
                return changeDate;
            }
            if (type === 'more') {
                if (range < 0) {
                    for (let i = Math.abs(range); i >= 0; i--) {
                        resultArray.push(formatDate(moment().utc().valueOf() + (-1000 * 3600 * 24 * i)));
                    }
                    return resultArray;
                }
                else {
                    for (let i = 1; i <= range; i++) {
                        resultArray.push(formatDate(moment().utc().valueOf() + (1000 * 3600 * 24 * i)));
                    }
                    return resultArray;
                }
            }
        }
    }
    else {
        changeDate = formatDate(moment().utc().valueOf() + (1000 * 3600 * 24 * range));
        return changeDate;
    }
};
//# sourceMappingURL=helper.js.map