"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ApiErrorCode;
(function (ApiErrorCode) {
    ApiErrorCode[ApiErrorCode["TIMEOUT"] = -1] = "TIMEOUT";
    ApiErrorCode[ApiErrorCode["SUCCESS"] = 10000] = "SUCCESS";
    ApiErrorCode[ApiErrorCode["USER_ID_INVALID"] = 10001] = "USER_ID_INVALID";
    ApiErrorCode[ApiErrorCode["USER_NAME_HAD"] = 10002] = "USER_NAME_HAD";
    ApiErrorCode[ApiErrorCode["USER_PASSWORD_ERROR"] = 10003] = "USER_PASSWORD_ERROR";
    ApiErrorCode[ApiErrorCode["REMOVE_FAILT"] = 10004] = "REMOVE_FAILT";
    ApiErrorCode[ApiErrorCode["USER_REGISTER_SUCCESS"] = 10005] = "USER_REGISTER_SUCCESS";
    ApiErrorCode[ApiErrorCode["USER_LOGIN_SUCCESS"] = 10006] = "USER_LOGIN_SUCCESS";
    ApiErrorCode[ApiErrorCode["USER_REQ"] = 10007] = "USER_REQ";
    ApiErrorCode[ApiErrorCode["VERIFICA_EMAIL_SUCCESS"] = 10008] = "VERIFICA_EMAIL_SUCCESS";
    ApiErrorCode[ApiErrorCode["VERIFICA_EMAIL_Failure"] = 10009] = "VERIFICA_EMAIL_Failure";
    ApiErrorCode[ApiErrorCode["VERIFICA_EMAIL_TOKEN"] = 10010] = "VERIFICA_EMAIL_TOKEN";
    ApiErrorCode[ApiErrorCode["NO_DATA"] = 10011] = "NO_DATA";
    ApiErrorCode[ApiErrorCode["USER_EMAIL_ERROR"] = 10012] = "USER_EMAIL_ERROR";
    ApiErrorCode[ApiErrorCode["REPLY_SUCCESS"] = 10013] = "REPLY_SUCCESS";
    ApiErrorCode[ApiErrorCode["DELETE_SUCCESS"] = 10014] = "DELETE_SUCCESS";
    ApiErrorCode[ApiErrorCode["PUBLISH_SUCCESS"] = 10015] = "PUBLISH_SUCCESS";
    ApiErrorCode[ApiErrorCode["PUBLISH_FAILURE"] = 10016] = "PUBLISH_FAILURE";
    ApiErrorCode[ApiErrorCode["CHANGE_USERINFO_FERROR"] = 10017] = "CHANGE_USERINFO_FERROR";
})(ApiErrorCode = exports.ApiErrorCode || (exports.ApiErrorCode = {}));
//# sourceMappingURL=api-error-code.enum.js.map