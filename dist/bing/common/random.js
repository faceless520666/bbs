"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const numbers = '0123456789';
const letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
const specials = '~!@#$%^*()_+-=[]{}|;:,./<>?';
exports.random = (length, options) => {
    length || (length = 8);
    options || (options = {});
    let chars = '';
    let result = '';
    if (options === true) {
        chars = `${numbers}${letters}${specials}`;
    }
    else if (typeof options === 'string') {
        chars = options;
    }
    else {
        if (options.number !== false) {
            chars += (typeof options.number === 'string') ? options.number : numbers;
        }
        if (options.letters !== false) {
            chars += (typeof options.letters === 'string') ? options.letters : letters;
        }
        if (options.specials) {
            chars += (typeof options.specials === 'string') ? options.specials : specials;
        }
    }
    while (length > 0) {
        length--;
        result += chars[Math.floor(Math.random() * chars.length)];
    }
    return result;
};
//# sourceMappingURL=random.js.map