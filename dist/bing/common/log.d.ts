export declare class Log {
    constructor();
    trace(msg: any): void;
    debug(msg: any): void;
    info(msg: any): void;
    warn(msg: any): void;
    error(msg: any, exception?: Error): void;
    fatal(msg: any): void;
    replaceConsole(): void;
    private initConfig;
    private writeLog;
    private execute;
    private getLogLevelName;
}
export declare enum LogLevel {
    Trace = 0,
    Debug = 1,
    Information = 2,
    Warning = 3,
    Error = 4,
    Fatal = 5
}
export declare class MessageWrapper {
    content: any;
    exception: Error;
}
