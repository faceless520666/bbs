"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const log4js_1 = require("log4js");
class Log {
    constructor() {
        this.initConfig();
    }
    trace(msg) {
        let outMsg = msg;
        if (msg === null) {
            outMsg = '';
        }
        const wrapper = new MessageWrapper();
        wrapper.content = outMsg;
        this.writeLog(wrapper, LogLevel.Trace);
    }
    debug(msg) {
        let outMsg = msg;
        if (msg === null) {
            outMsg = '';
        }
        const wrapper = new MessageWrapper();
        wrapper.content = outMsg;
        this.writeLog(wrapper, LogLevel.Debug);
    }
    info(msg) {
        let outMsg = msg;
        if (msg === null) {
            outMsg = '';
        }
        const wrapper = new MessageWrapper();
        wrapper.content = outMsg;
        this.writeLog(wrapper, LogLevel.Information);
    }
    warn(msg) {
        let outMsg = msg;
        if (msg === null) {
            outMsg = '';
        }
        const wrapper = new MessageWrapper();
        wrapper.content = outMsg;
        this.writeLog(wrapper, LogLevel.Warning);
    }
    error(msg, exception) {
        let outMsg = msg;
        if (msg === null) {
            outMsg = '';
        }
        const wrapper = new MessageWrapper();
        wrapper.content = outMsg;
        wrapper.exception = exception;
        this.writeLog(wrapper, LogLevel.Error);
    }
    fatal(msg) {
        let outMsg = msg;
        if (msg === null) {
            outMsg = '';
        }
        const wrapper = new MessageWrapper();
        wrapper.content = outMsg;
        this.writeLog(wrapper, LogLevel.Fatal);
    }
    replaceConsole() {
        const logger = log4js_1.getLogger('console');
        console.log = logger.info.bind(logger);
    }
    initConfig() {
        log4js_1.configure('./config/log4js.json');
    }
    writeLog(msg, level) {
        const levelName = this.getLogLevelName(level);
        if (levelName === '') {
            return;
        }
        const logger = log4js_1.getLogger(levelName);
        this.execute(msg, logger, level);
    }
    execute(msg, logger, level) {
        switch (level) {
            case LogLevel.Trace:
                if (logger.isTraceEnabled()) {
                    logger.trace(msg.content);
                }
                break;
            case LogLevel.Debug:
                if (logger.isDebugEnabled()) {
                    logger.debug(msg.content);
                }
                break;
            case LogLevel.Information:
                if (logger.isInfoEnabled()) {
                    logger.info(msg.content);
                }
                break;
            case LogLevel.Warning:
                if (logger.isWarnEnabled()) {
                    logger.warn(msg.content);
                }
                break;
            case LogLevel.Error:
                if (logger.isErrorEnabled()) {
                    logger.error(msg.content, msg.exception);
                }
                break;
            case LogLevel.Fatal:
                if (logger.isFatalEnabled()) {
                    logger.fatal(msg.content);
                }
                break;
            default:
                break;
        }
    }
    getLogLevelName(level) {
        switch (level) {
            case LogLevel.Trace:
                return 'trace';
            case LogLevel.Debug:
                return 'debug';
            case LogLevel.Information:
                return 'information';
            case LogLevel.Error:
                return 'error';
            case LogLevel.Fatal:
                return 'fatal';
            default:
                return '';
        }
    }
}
exports.Log = Log;
var LogLevel;
(function (LogLevel) {
    LogLevel[LogLevel["Trace"] = 0] = "Trace";
    LogLevel[LogLevel["Debug"] = 1] = "Debug";
    LogLevel[LogLevel["Information"] = 2] = "Information";
    LogLevel[LogLevel["Warning"] = 3] = "Warning";
    LogLevel[LogLevel["Error"] = 4] = "Error";
    LogLevel[LogLevel["Fatal"] = 5] = "Fatal";
})(LogLevel = exports.LogLevel || (exports.LogLevel = {}));
class MessageWrapper {
}
exports.MessageWrapper = MessageWrapper;
//# sourceMappingURL=log.js.map