"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
class DataType {
    static trancdate() {
        return __awaiter(this, void 0, void 0, function* () {
            const date = new Date();
            const yy = date.getFullYear();
            const mon = date.getMonth() > 8 ? date.getMonth() + 1 : '0' + (date.getMonth() + 1);
            const dd = date.getDate() > 9 ? date.getDate() : '0' + date.getDate();
            const hh = date.getHours() > 9 ? date.getHours() : '0' + date.getHours();
            const mm = date.getMinutes() > 9 ? date.getMinutes() : '0' + date.getMinutes();
            const ss = date.getSeconds() > 9 ? date.getSeconds() : '0' + date.getSeconds();
            return yy + '-' + mon + '-' + dd + ' ' + hh + ':' + mm + ':' + ss;
        });
    }
    static toSecond() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.trancdate();
        });
    }
    static toDay() {
        return __awaiter(this, void 0, void 0, function* () {
            const toDay = yield this.trancdate();
            const returnDay = toDay.substring(0, 10);
            return returnDay;
        });
    }
    static getTime() {
        const date = new Date();
        return date.getTime();
    }
}
exports.DataType = DataType;
//# sourceMappingURL=dateType.js.map