"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const multer = require("multer");
const fs = require("fs");
exports.uploadConfig = {
    fileFilter: (req, file, cb) => {
        let extension = (file.originalname.split('.').pop());
        cb(null, true);
    },
    limits: {
        fileSize: 209715200
    },
    storage: multer.diskStorage({
        destination: function (req, file, cb) {
            var _path = path.join(__dirname, "../../../static_files");
            if (!fs.existsSync(_path)) {
                fs.mkdirSync(_path);
            }
            cb(null, _path);
        },
        filename: function (req, file, cb) {
            cb(null, file.originalname);
        }
    })
};
//# sourceMappingURL=uploadConfig.js.map