export declare class DataType {
    static trancdate(): Promise<string>;
    static toSecond(): Promise<string>;
    static toDay(): Promise<string>;
    static getTime(): number;
}
