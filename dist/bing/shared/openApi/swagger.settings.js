"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class SwaggerSettings {
    constructor() {
        this.version = '1.0';
        this.viewPath = 'swagger';
        this.basePath = 'api';
    }
}
exports.SwaggerSettings = SwaggerSettings;
//# sourceMappingURL=swagger.settings.js.map