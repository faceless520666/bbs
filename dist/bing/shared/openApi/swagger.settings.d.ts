export declare class SwaggerSettings {
    title: string;
    description: string;
    version: string;
    viewPath: string;
    basePath: string;
}
