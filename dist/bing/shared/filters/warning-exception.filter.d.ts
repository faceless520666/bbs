import { util } from '../../../bing';
import { Warning } from '../../../bing/core/warning';
import { ArgumentsHost, ExceptionFilter } from '@nestjs/common';
export declare class WarningExceptionFilter implements ExceptionFilter {
    protected util: typeof util;
    catch(exception: Warning, host: ArgumentsHost): void;
}
