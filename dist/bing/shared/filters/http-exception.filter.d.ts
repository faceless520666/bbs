import { util } from '../../../bing';
import { ArgumentsHost, ExceptionFilter, HttpException } from '@nestjs/common';
export declare class HttpExcetpionFilter implements ExceptionFilter {
    protected util: typeof util;
    catch(exception: HttpException, host: ArgumentsHost): void;
}
