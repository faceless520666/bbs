import { util } from '../../../bing';
import { ArgumentsHost, ExceptionFilter } from '@nestjs/common';
export declare class GlobalExceptionFilter implements ExceptionFilter {
    protected util: typeof util;
    catch(exception: any, host: ArgumentsHost): void;
}
