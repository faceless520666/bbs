"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const bing_1 = require("../../../bing");
const warning_1 = require("../../../bing/core/warning");
const common_1 = require("@nestjs/common");
let WarningExceptionFilter = class WarningExceptionFilter {
    constructor() {
        this.util = bing_1.util;
    }
    catch(exception, host) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse();
        const request = ctx.getRequest();
        this.util.log.warn({
            title: '应用程序异常',
            exception,
            request: {
                headers: request.headers,
                method: request.method,
                url: request.url,
                originalUrl: request.originalUrl,
                params: request.params,
                query: request.query,
                body: request.body,
                route: request.route,
                ip: request.ip
            },
        });
        response
            .status(200)
            .json({
            code: bing_1.util.helper.toInt(exception.code),
            message: exception.message,
            operationTime: bing_1.util.moment().utc().format('YYYY-MM-DD hh:mm:ss.SSS'),
        });
    }
};
WarningExceptionFilter = __decorate([
    common_1.Catch(warning_1.Warning)
], WarningExceptionFilter);
exports.WarningExceptionFilter = WarningExceptionFilter;
//# sourceMappingURL=warning-exception.filter.js.map