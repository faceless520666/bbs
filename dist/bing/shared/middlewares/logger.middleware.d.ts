import { MiddlewareFunction, NestMiddleware } from '@nestjs/common';
import { util } from '../../../bing';
export declare class LoggerMiddleware implements NestMiddleware {
    protected util: typeof util;
    resolve(...args: any[]): Promise<MiddlewareFunction>;
}
