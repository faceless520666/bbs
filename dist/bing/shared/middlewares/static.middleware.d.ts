import { MiddlewareFunction, NestMiddleware } from '@nestjs/common';
export declare class StaticMiddleware implements NestMiddleware {
    resolve(...args: any[]): Promise<MiddlewareFunction>;
}
