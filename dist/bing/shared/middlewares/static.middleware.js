"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const fs = require("fs");
const path = require("path");
let StaticMiddleware = class StaticMiddleware {
    resolve(...args) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('执行静态目录中间件');
            return (req, res, next) => __awaiter(this, void 0, void 0, function* () {
                console.log('执行静态目录中间件0001');
                const fileNameArray = yield readDirFunc(path.resolve('./download'));
                let targetFile;
                for (const filename of fileNameArray) {
                    if ('/' + filename === req.url) {
                        targetFile = req.url;
                        break;
                    }
                }
                if (!targetFile) {
                    next();
                }
                else {
                    res.header('Content-Type', 'text/html');
                    return res.sendFile(path.resolve(`./download${targetFile}`));
                }
            });
        });
    }
};
StaticMiddleware = __decorate([
    common_1.Injectable()
], StaticMiddleware);
exports.StaticMiddleware = StaticMiddleware;
const readDirFunc = (dirPath) => {
    return new Promise((resolve, reject) => {
        fs.readdir(dirPath, (err, files) => {
            if (err) {
                throw err;
            }
            resolve(files);
        });
    });
};
//# sourceMappingURL=static.middleware.js.map