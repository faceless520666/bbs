"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const cls = require("cls-hooked");
class RequestContext {
    constructor(request, response) {
        this.id = Math.random();
        this.request = request;
        this.response = response;
    }
    static currentRequestContext() {
        const session = cls.getNamespace(RequestContext.nsid);
        if (session && session.active) {
            return session.get(RequestContext.name);
        }
        return null;
    }
    static currentRequest() {
        const requestContext = RequestContext.currentRequestContext();
        if (requestContext) {
            return requestContext.request;
        }
        return null;
    }
    static currentUser(throwError) {
        const requestContext = RequestContext.currentRequestContext();
        if (requestContext) {
            const user = requestContext.request['user'];
            if (user) {
                return user;
            }
        }
        if (throwError) {
            throw new common_1.HttpException('Unauthorized', common_1.HttpStatus.UNAUTHORIZED);
        }
        return null;
    }
}
RequestContext.nsid = 'some_random_guid';
exports.RequestContext = RequestContext;
//# sourceMappingURL=request-context.js.map