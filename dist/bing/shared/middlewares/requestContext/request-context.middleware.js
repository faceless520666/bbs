"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const cls = require("cls-hooked");
const request_context_1 = require("./request-context");
let RequestContextMidlleware = class RequestContextMidlleware {
    resolve() {
        console.log('执行请求上下文中间件...');
        return (req, res, next) => {
            const requestContext = new request_context_1.RequestContext(req, res);
            const session = cls.getNamespace(request_context_1.RequestContext.nsid) || cls.createNamespace(request_context_1.RequestContext.nsid);
            session.run(() => __awaiter(this, void 0, void 0, function* () {
                session.set(request_context_1.RequestContext.name, requestContext);
                next();
            }));
        };
    }
};
RequestContextMidlleware = __decorate([
    common_1.Injectable()
], RequestContextMidlleware);
exports.RequestContextMidlleware = RequestContextMidlleware;
//# sourceMappingURL=request-context.middleware.js.map