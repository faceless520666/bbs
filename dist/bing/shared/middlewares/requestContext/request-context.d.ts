/// <reference types="node" />
import { IncomingMessage } from 'http';
export declare class RequestContext {
    static nsid: string;
    readonly id: Number;
    request: IncomingMessage;
    response: Response;
    constructor(request: IncomingMessage, response: Response);
    static currentRequestContext(): RequestContext;
    static currentRequest(): IncomingMessage;
    static currentUser(throwError?: boolean): any;
}
