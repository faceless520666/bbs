import { NestMiddleware } from '@nestjs/common';
export declare class RequestContextMidlleware implements NestMiddleware {
    resolve(): (req: any, res: any, next: any) => void;
}
