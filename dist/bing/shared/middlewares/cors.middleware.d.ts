import { NestMiddleware, MiddlewareFunction } from '@nestjs/common';
export declare class CorsMiddleware implements NestMiddleware {
    resolve(): Promise<MiddlewareFunction>;
}
