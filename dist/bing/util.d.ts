import * as helper from './common/helper';
import * as encrypt from './common/encrypt';
import { Log } from './common/log';
import moment = require('moment');
import * as random from './common/random';
import { DataType } from './common/dateType';
export declare class Util {
    static helper: typeof helper;
    static log: Log;
    static encrypt: typeof encrypt;
    static moment: typeof moment;
    static ramdom: typeof random;
    static dateType: typeof DataType;
    static session: {
        secrets: string;
    };
    static client: any;
}
