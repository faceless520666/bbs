"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const swagger_1 = require("@nestjs/swagger");
const global_exception_filter_1 = require("./shared/filters/global-exception.filter");
const http_exception_filter_1 = require("./shared/filters/http-exception.filter");
const warning_exception_filter_1 = require("./shared/filters/warning-exception.filter");
const validation_pipe_1 = require("./shared/pipes/validation.pipe");
const result_wrapper_interceptor_1 = require("./shared/interceptors/result-wrapper.interceptor");
const bodyParser = require("body-parser");
const log4js_1 = require("log4js");
const express = require("express");
const path = require("path");
class BingStartup {
    static startup(app) {
        app.useGlobalPipes(new validation_pipe_1.ValidationPipe());
        app.useGlobalFilters(new global_exception_filter_1.GlobalExceptionFilter(), new http_exception_filter_1.HttpExcetpionFilter(), new warning_exception_filter_1.WarningExceptionFilter());
        app.useGlobalInterceptors(new result_wrapper_interceptor_1.ResultWrapperInterceptor());
        app.use(bodyParser.json());
        app.enableCors();
        app.use(log4js_1.connectLogger(log4js_1.getLogger('http'), {
            level: 'auto',
            format: (req, res, format) => format(`日志时间：:date  请求地址：:url  请求方法：:method \r\n客户端IP地址：:remote-addr \r\n引用地址：:referrer \r\n客户端信息：:user-agent \r\n响应状态：:status \r\n响应时间：:response-time`)
        }));
        app.use(express.static(path.join(__dirname, '../libs')));
        app.use(express.static(path.join(__dirname, '../../static_files/')));
        app.set('views', path.join(__dirname, '../views'));
        app.set('view engine', 'ejs');
    }
    static configSwagger(settings, app) {
        const options = new swagger_1.DocumentBuilder()
            .setTitle(settings.title)
            .setDescription(settings.description)
            .setVersion(settings.version)
            .setBasePath(settings.basePath)
            .build();
        const document = swagger_1.SwaggerModule.createDocument(app, options);
        swagger_1.SwaggerModule.setup(settings.viewPath, app, document);
    }
}
exports.BingStartup = BingStartup;
//# sourceMappingURL=bing-startup.js.map