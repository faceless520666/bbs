import { INestApplication, INestExpressApplication } from '@nestjs/common';
import { SwaggerSettings } from './shared/openApi/swagger.settings';
export declare class BingStartup {
    static startup(app: INestApplication & INestExpressApplication): void;
    static configSwagger(settings: SwaggerSettings, app: INestApplication): void;
}
