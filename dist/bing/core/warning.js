"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Warning extends Error {
    constructor(message, code = '2') {
        super(message);
        this.message = message;
        this.code = code;
    }
}
exports.Warning = Warning;
//# sourceMappingURL=warning.js.map