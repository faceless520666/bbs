import { PassiveException } from './passive.exception';
export declare class ValidationException extends PassiveException {
    constructor(message?: string);
}
