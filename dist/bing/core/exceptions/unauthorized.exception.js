"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const passive_exception_1 = require("./passive.exception");
const common_1 = require("@nestjs/common");
class UnauthorizedException extends passive_exception_1.PassiveException {
    constructor(message) {
        super(message || '无效授权', common_1.HttpStatus.UNAUTHORIZED);
    }
}
exports.UnauthorizedException = UnauthorizedException;
//# sourceMappingURL=unauthorized.exception.js.map