"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const passive_exception_1 = require("./passive.exception");
class ValidationException extends passive_exception_1.PassiveException {
    constructor(message) {
        super(message || '请求格式无效', common_1.HttpStatus.BAD_REQUEST);
    }
}
exports.ValidationException = ValidationException;
//# sourceMappingURL=validation.exception.js.map