"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
class ExceptionBase extends common_1.HttpException {
    constructor(response, status, error) {
        super(response, status);
        this.error = error;
    }
}
exports.ExceptionBase = ExceptionBase;
//# sourceMappingURL=exception.base.js.map