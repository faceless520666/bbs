import { PassiveException } from './passive.exception';
export declare class BadRequestException extends PassiveException {
    constructor(message?: any);
}
