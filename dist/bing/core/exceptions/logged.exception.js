"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const exception_base_1 = require("./exception.base");
class LoggedException extends exception_base_1.ExceptionBase {
    constructor(response, status, error) {
        super(response, status, error);
    }
}
exports.LoggedException = LoggedException;
//# sourceMappingURL=logged.exception.js.map