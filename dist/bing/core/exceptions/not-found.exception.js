"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const passive_exception_1 = require("./passive.exception");
const common_1 = require("@nestjs/common");
class NotFoundException extends passive_exception_1.PassiveException {
    constructor(message) {
        super(message || '所请求的页面不存在或已被删除', common_1.HttpStatus.NOT_FOUND);
    }
}
exports.NotFoundException = NotFoundException;
//# sourceMappingURL=not-found.exception.js.map