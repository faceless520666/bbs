import { PassiveException } from './passive.exception';
export declare class UnauthorizedException extends PassiveException {
    constructor(message?: any);
}
