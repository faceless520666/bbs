"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const exception_base_1 = require("./exception.base");
class PassiveException extends exception_base_1.ExceptionBase {
    constructor(response, status, error) {
        super(response, status, error);
    }
}
exports.PassiveException = PassiveException;
//# sourceMappingURL=passive.exception.js.map