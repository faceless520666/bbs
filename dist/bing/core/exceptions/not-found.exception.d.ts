import { PassiveException } from "./passive.exception";
export declare class NotFoundException extends PassiveException {
    constructor(message?: string);
}
