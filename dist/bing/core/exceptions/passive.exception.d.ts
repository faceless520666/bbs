import { ExceptionBase } from "./exception.base";
export declare abstract class PassiveException extends ExceptionBase {
    constructor(response: string | object, status: number, error?: Error);
}
