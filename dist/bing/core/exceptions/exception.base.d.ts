import { HttpException } from "@nestjs/common";
export declare abstract class ExceptionBase extends HttpException {
    error: Error;
    constructor(response: string | object, status: number, error?: Error);
}
