"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Result {
}
exports.Result = Result;
var StateCode;
(function (StateCode) {
    StateCode[StateCode["Ok"] = 1] = "Ok";
    StateCode[StateCode["Fail"] = 2] = "Fail";
})(StateCode = exports.StateCode || (exports.StateCode = {}));
//# sourceMappingURL=result.js.map