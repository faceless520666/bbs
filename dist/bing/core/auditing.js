"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AuditedMethod;
(function (AuditedMethod) {
    AuditedMethod[AuditedMethod["Created"] = 0] = "Created";
    AuditedMethod[AuditedMethod["Updated"] = 1] = "Updated";
    AuditedMethod[AuditedMethod["Deleted"] = 2] = "Deleted";
})(AuditedMethod = exports.AuditedMethod || (exports.AuditedMethod = {}));
//# sourceMappingURL=auditing.js.map