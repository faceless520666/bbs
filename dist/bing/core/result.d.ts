export declare class Result<T> {
    code: number | StateCode;
    message: string;
    data: T;
    operationTime: string;
}
export declare enum StateCode {
    Ok = 1,
    Fail = 2
}
