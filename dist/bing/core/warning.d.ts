export declare class Warning extends Error {
    readonly message: string;
    code: string;
    constructor(message: string, code?: string);
}
