export interface IKey {
    id: string;
}
export interface IVersion {
    version: string;
}
export interface IDelete {
    isDeleted: boolean;
}
export declare class ViewModel implements IKey {
    id: string;
}
export declare class QueryParameter {
    page: number;
    limit: number;
    order: string;
    keyword: string;
}
