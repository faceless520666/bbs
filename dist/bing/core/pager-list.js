"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bing_1 = require("../../bing");
class PagerList {
    constructor(list) {
        if (!list) {
            return;
        }
        this.page = list.page;
        this.pageSize = list.pageSize;
        this.totalCount = list.totalCount;
        this.pageCount = list.pageCount;
        this.order = list.order;
        this.data = list.data;
    }
    initData(list, queryParameter, totalCount) {
        this.data = list;
        this.page = bing_1.util.helper.toInt(queryParameter.page);
        this.pageSize = bing_1.util.helper.toInt(queryParameter.limit);
        this.totalCount = bing_1.util.helper.toInt(totalCount);
        this.initPageCount();
    }
    initPageCount() {
        if ((this.totalCount % this.pageSize) === 0) {
            this.pageCount = bing_1.util.helper.toInt(this.totalCount / this.pageSize);
            return;
        }
        this.pageCount = bing_1.util.helper.floor((this.totalCount / this.pageSize) + 1);
    }
}
exports.PagerList = PagerList;
//# sourceMappingURL=pager-list.js.map