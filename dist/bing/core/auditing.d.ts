export interface ICreationAudited {
    creationTime: Date;
    creatorId: string;
}
export interface IModificationAudited {
    lastModificationTime: Date;
    lastModifierId: string;
}
export interface IDeletionAudited {
    deletionTime: Date;
    deleterId: string;
}
export interface IAudited extends ICreationAudited, IModificationAudited {
}
export interface IFullAudited extends IAudited, IDeletionAudited {
}
export declare enum AuditedMethod {
    Created = 0,
    Updated = 1,
    Deleted = 2
}
