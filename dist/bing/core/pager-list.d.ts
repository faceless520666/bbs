import { QueryParameter } from './model';
export declare class PagerList<T> {
    page: number;
    pageSize: number;
    totalCount: number;
    pageCount: number;
    order: string;
    data: T[];
    constructor(list?: PagerList<T>);
    initData(list: T[], queryParameter: QueryParameter, totalCount: number): void;
    private initPageCount;
}
