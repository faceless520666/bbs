"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const bing_1 = require("./bing");
const typeorm_1 = require("@nestjs/typeorm");
const app_1 = require("./app");
const entitys_1 = require("./app/entitys");
let ApplicationModule = class ApplicationModule {
    configure(consumer) {
        consumer.apply(bing_1.CorsMiddleware).forRoutes({
            path: '*',
            method: common_1.RequestMethod.ALL,
        });
    }
};
ApplicationModule = __decorate([
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forRoot({
                type: 'mysql',
                host: '7.7.2.11',
                port: 3306,
                username: 'root',
                password: 'p82Jser2Sb32F2sl4Gsir02Fe',
                database: 'example',
                entities: [
                    entitys_1.BbsArticleDetail,
                    entitys_1.BbsChildrenComments,
                    entitys_1.BbsCommentsList,
                    entitys_1.BbsLabelList,
                    entitys_1.BbsLabelType,
                    entitys_1.BbsMenu,
                    entitys_1.BbsMyCollectionList,
                    entitys_1.BbsMyLikeList,
                    entitys_1.BbsPostList,
                    entitys_1.BbsUser,
                ],
                synchronize: true,
            }), ...app_1.default,
        ],
    })
], ApplicationModule);
exports.ApplicationModule = ApplicationModule;
//# sourceMappingURL=app.module.js.map